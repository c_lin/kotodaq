#!/bin/bash

TOP_slot=3
FAN_slot=5
OFC_slot=7

vme="/root/vme7700"
e14vme="/misc/dev/executable/diagnosis/e14vme/bin/e14vme"

# switch to test mode for TOP CDT and FANOUT-II CDT
echo -e "switch to test mode."
${vme} write ${TOP_slot} 3c 1
${vme} write ${FAN_slot} 3c 1
${vme} write ${OFC_slot} 3c 1

# read version first
echo -e "version code TOP / FANOUT / OFC1"
${vme} read ${TOP_slot} 0
${vme} read ${FAN_slot} 0
${vme} read ${OFC_slot} 0

# 
echo -e "Before starting the test, count should be ffff."
${vme} write ${OFC_slot} 4 80000000 
${vme} write ${FAN_slot} 44 1
# addr 100 [15:0] input#0
${vme} read ${FAN_slot} 400

# sending trigger
echo -e "Start consecutive numbers from OFC-1"
${vme} write ${OFC_slot} 44 1
# register #1 [31:30] = 1: consecutive integers for debugging
${vme} write ${OFC_slot} 4 40000000
sleep 1

# check numbers
# 0: lv1 trigger count
echo -e "FANOUT CDT number of errors (should be 0) = "
echo -e "                            (max err = 0xfffe, no input = 0xffff) "
${vme} read ${FAN_slot} 400

# switch back to standard error output signal
${vme} write ${OFC_slot} 4 0
