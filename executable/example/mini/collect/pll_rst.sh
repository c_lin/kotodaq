
e14vme="/misc/dev/executable/diagnosis/e14vme/bin/e14vme"

slot=3
${e14vme} write tcdt ${slot} pll_reset 1

slot=5
${e14vme} write f2cdt ${slot} pll_reset 1

slot=7
${e14vme} write ofc1 ${slot} pll_reset 1

slot=9
${e14vme} write adc ${slot} pll_reset 1
${e14vme} write adc ${slot} pll_reset 0

slot=10
${e14vme} write adc ${slot} pll_reset 1
${e14vme} write adc ${slot} pll_reset 0

slot=11
${e14vme} write adc ${slot} pll_reset 1
${e14vme} write adc ${slot} pll_reset 0
