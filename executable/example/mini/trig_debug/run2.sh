#!/bin/bash

TOP_slot=3
FAN_slot=5
OFC_slot=7

vme="/root/vme7700"
e14vme="/misc/dev/executable/diagnosis/e14vme/bin/e14vme"

# switch to test mode for TOP CDT and FANOUT-II CDT
echo -e "switch to test mode."
${vme} write ${TOP_slot} 3c 1
${vme} write ${FAN_slot} 3c 1
${vme} write ${OFC_slot} 3c 1

# read version first
echo -e "version code TOP / FANOUT / OFC1"
${vme} read ${TOP_slot} 0
${vme} read ${FAN_slot} 0
${vme} read ${OFC_slot} 0

# 
echo -e "Before sending triggers, count should be 0."
${vme} write ${FAN_slot} 44 1
${vme} write ${FAN_slot} c 0
echo -e "FANOUT CDT number of Lv1 triggers = "
${vme} read ${FAN_slot} 2c

${vme} write ${OFC_slot} 44 1

# addr 20, val =0 #L1A count
${vme} write ${OFC_slot} 80 0
echo -e "OFC1 number of Lv1 trigger = "
# addr 101, result of count
${vme} read ${OFC_slot} 404  

# set parameters
${e14vme} write tcdt ${TOP_slot} "factory_trig_ptn" 0
${e14vme} write tcdt ${TOP_slot} "factory_trig_gap" 1
${e14vme} write tcdt ${TOP_slot} "factory_ntrig" 60000

# sending trigger
echo -e "Start sending triggers"
${vme} write ${FAN_slot} 44 1
${vme} write ${OFC_slot} 44 1
${vme} write ${TOP_slot} 44 1
${e14vme} write tcdt ${TOP_slot} "send_factory_trig" 1
sleep 2
${e14vme} write tcdt ${TOP_slot} "send_factory_trig" 0


# check numbers
# 0: lv1 trigger count
sleep 1
${vme} write ${FAN_slot} c 0
echo -e "FANOUT CDT number of Lv1 triggers (should be EA60) = "
${vme} read ${FAN_slot} 2c 

${vme} write ${OFC_slot} 80 0
echo -e "OFC-1 number of Lv1 triggers (should be EA60) = "
${vme} read ${OFC_slot} 404
