#!/bin/bash

phase=0

TOP_slot=3
FAN_slot=5

vme="/root/vme7700"
e14vme="/misc/dev/executable/diagnosis/e14vme/bin/e14vme"

# switch to test mode for TOP CDT and FANOUT-II CDT
echo -e "switch to test mode."
${vme} write ${TOP_slot} 3c 1
${vme} write ${FAN_slot} 3c 1

# read version first
echo -e "version code TOP / FANOUT"
${vme} read ${TOP_slot} 0
${vme} read ${FAN_slot} 0

# stop sending triggers before resetting
${e14vme} write tcdt ${TOP_slot} "fanout_clk_sw" ${phase}

# read back the phase
echo -e "read back phase:  "
${e14vme} read tcdt ${TOP_slot} "fanout_clk_sw"
