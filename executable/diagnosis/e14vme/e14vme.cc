#include <cstdio>
#include <iostream>
#include <string>
#include "VMEController.h"

void arg_err( void ){
   std::cout<<" Argument should be : (e14vme) read (boardname) (slot) (reg)" << std::endl;
   std::cout<<"                      (e14vme) write (boardname) (slot) (reg) (val)" << std::endl;
   std::cout<<" Valid board name: adc, tcdt, fcdt, lcdt, eofc, cofc " << std::endl;
   std::cout<<" VME Write (val) is input as decimal number " << std::endl;
}

int main(int argc, char* argv[]){

  std::cout<<std::hex;

  if( argc < 5 || argc > 6 ) 
  {
     std::cout<<" Argument Number Error\n";
     arg_err();
  }
  else
  {
     int slot = std::atoi( argv[3] );
     VMEController* bd = new VMEController(slot,argv[2]);
     if( !(bd->IsBoardRegistered()) ){
        std::cout<<" VME Board Start :: Not Registered ! \n";
        arg_err();
        return 0;
     }
 
     if( strcmp(argv[1], "read" ) == 0 ){
        std::cout<< argv[4] << std::endl;
        std::cout<< bd->read_reg( argv[4] ) << std::endl;
     }
     else if( strcmp(argv[1], "write" ) == 0 ){
        unsigned long new_val = std::atoi(argv[5]);  //if(new_val==0x7fffffff){new_val++;}
        int RWstatus = 0, trial_cnt = 0;
        RWstatus = bd->write_reg( argv[4] , new_val );
	unsigned long rd_val = bd->read_reg(argv[4]);
//	std::cout << " Writing Status: " << RWstatus << "\nWrite Value: " << new_val << ",\tRead Value: " << rd_val << std::endl;

/*
	while( new_val != rd_val ){
	  RWstatus = bd->write_reg( argv[4] , new_val );
	  std::cout<<" Register writing error : " << argv[4]
		   <<" . Intend to Write: " << std::hex << new_val << ", Got: " << rd_val 
                   <<" for board " << std::dec << argv[2] << " in slot " << slot
                   <<" ( trial # : " << ++trial_cnt << " ) " << std::endl;
	  if(RWstatus==1) {std::cout<<" - Writing Status seemed successful...\n";}
	  else {std::cout<<" - Writing Status seemed NOT successful...\n";}
	  std::cout<<" Now rewrite ... " << std::endl;
	  rd_val = bd->read_reg(argv[4]);
	}
*/     
     }
     else 
     {
        std::cout<<" Argument Number Error\n";
        arg_err();
     }

   }

  return 0;

}

