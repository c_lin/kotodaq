#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <bitset>

#include "Register.h"
#include "VMEController.h"
#include "TopCDTManager.h"
#include "FADCManager.h"

int main()
{
   const int nspill = 100;

   std::cout<<" initialization... " << std::endl;
   TopCDTManager  tcdt(3);
   VMEController  f2cdt(5,"f2cdt");
   VMEController  ofc1(7,"ofc1");

//   VMEController  ofc1_2(11,"ofc1");
   FADCManager    fadc1(9,0);      
   FADCManager    fadc2(10,0);      
   FADCManager    fadc3(11,0);     
/* 
   FADCManager    fadc4(14,0);      
*/
   std::cout<<" TOP       CDT version: " << std::hex << tcdt.GetVersion()  << std::endl;
   std::cout<<" FANOUT-II CDT version: " << std::hex << f2cdt.GetVersion() << std::endl;
   std::cout<<" OFC-I         version: " << std::hex << ofc1.GetVersion()  << std::endl;

//   std::cout<<" OFC-I (2)     version: " << std::hex << ofc1_2.GetVersion()  << std::endl;
   std::cout<<" FADC1         version: " << std::dec << fadc1.GetVersion() << std::endl;
   std::cout<<" FADC2         version: " << std::dec << fadc2.GetVersion() << std::endl;
   std::cout<<" FADC3         version: " << std::dec << fadc3.GetVersion() << std::endl;
/*
   std::cout<<" FADC4         version: " << std::dec << fadc4.GetVersion() << std::endl;
*/
   // OFC-I setup
   ofc1.write_reg("test_mode",1);
   ofc1.write_reg("reset",1);

   /// enable rx [17..0]
   ofc1.write_word(0x8c,0x30001);
   //ofc1.write_reg("ena_olrx",0x1);
   ofc1.write_reg("input_mask",0xfffe);
/*
   ofc1_2.write_reg("test_mode",1);
   ofc1_2.write_reg("reset",1);
   ofc1_2.write_reg("ena_olrx",0x3);
   ofc1_2.write_reg("input_mask",0xfffc);
*/
   // FANOUT CDT setup
   f2cdt.write_reg("reset",0x1);
   f2cdt.write_reg("lvds_tx_sw",0x0);

   //!!!!!!!!!!!!!!!!!
   // DISABLE ERROR //
   f2cdt.write_reg("err_ena",0x1);

   // TOP CDT setup
   tcdt.SetStandaloneMode(true);
   tcdt.SetTestMode(true);

   tcdt.write_reg("live_mode",0);

// temporarily commented
   tcdt.write_reg("ena_ofc1_err",0x1);

   /////
   tcdt.write_reg("delta_veto_ptn", 0x0);
   tcdt.write_reg("delta_et_thr",0x1);
   tcdt.write_reg("et_thr",0xFFFF);
   //tcdt.write_reg("et_thr",0x1);
   
   /// disable veto inputs (> v40.00.06)
   tcdt.write_reg("fiber_in_sw",0x6);

   tcdt.write_reg("reset",1);
   tcdt.write_reg("nspill",nspill);

   tcdt.write_reg("start_delay",2);

   tcdt.write_reg("internal_live_period", 120);
   tcdt.write_reg("live_length", 100); 

   tcdt.write_reg("bypass_tlk_err",1);
   tcdt.write_reg("bypass_dc_err",1);
   tcdt.write_reg("bypass_local_align",1);
   tcdt.write_reg("bypass_global_align",1);

   tcdt.write_reg("ext3_ena",1);
   tcdt.write_reg("ext3_spill_on",1);
   tcdt.write_reg("ext3_spill_off",1);

//   tcdt.SetInternalClock(0xfffff);
   tcdt.SetInternalClock(0);
 
   fadc1.Init();

   fadc1.SetAnalysisMethod(e14::k_ET);
   fadc1.SetDaisyChain(3);
   fadc1.write_reg("slotID",9);
   fadc1.write_reg("crateID",1);

   fadc2.Init();
   fadc2.write_reg("slotID",10);    
   fadc2.write_reg("crateID",1);

   fadc3.Init();
   fadc3.write_reg("slotID",11);
   fadc3.write_reg("crateID",1);
/*
   fadc4.Init();
*/
   std::cout<<" operation mode. " << std::endl;
   ofc1.write_reg("test_mode",0);
   //ofc1_2.write_reg("test_mode",0);
   tcdt.SetTestMode(false);

   std::cout<<std::dec;

   int spill_cnt = 0;
   bool prelive = false;

   int timer = 0;

   while( spill_cnt < nspill )
   {
      bool rlive  = tcdt.read_reg("live");
      if( rlive == 1 && prelive == 0 ){
         timer = 0;
      }

      if( timer==10 ){
         spill_cnt =  tcdt.read_reg("spill_count") ;
         std::cout <<" spill no. = " << spill_cnt << std::endl;
         std::cout <<" === spill cycle length = " << tcdt.read_reg("spill_cycle_length") 
                   * 0.067 << " (sec)" << std::endl;    
         std::cout <<" delta ET pattern: " << std::hex 
                   << tcdt.read_reg("delta_et_raw") << std::endl;
         //std::cout <<" delta ET delay time: " << std::dec 
         //          << tcdt.read_reg("delay_et_delta_ptn") << std::endl;
         std::cout <<" OFC-I TLK err result : " << std::hex
                   << ofc1.read_reg("tlk_err") << std::endl;
//         std::cout <<" OFC-I energy word err? : " << std::hex
//                   << ofc1.read_reg("eneword_err") << std::endl;

//         std::cout << " OFC-I 00_nqueue = " << ofc1.read_word(0x24) << std::endl;
       }

       std::cout<<std::dec;

       if( rlive == 0 && prelive == 1 ){
         std::cout <<" OFC-I event no err  : " << std::hex
                   << ofc1.read_reg("evtno_err") << std::endl;
         std::cout <<" OFC-I spill no result : " << std::hex
                   << ofc1.read_reg("spillno_err") << std::endl;
//         std::cout <<" OFC-I pending err: " << std::hex 
//                   << ofc1.read_reg("pending_err") << std::endl;
//         std::cout <<" OFC-I package overflow: " << std::hex
//                   << ofc1.read_reg("pkg_overflow") << std::endl;
//         std::cout <<" OFC-I stop: " << std::hex
//                   << ofc1.read_reg("stop") << std::endl;

/*
         std::cout <<" FANOUT2 CDT error status : "
                   << f2cdt.read_reg("err_status") << std::endl;
         std::cout <<" FANOUT2 CDT stop status : "
                   << f2cdt.read_reg("stop_status") << std::endl;
         std::cout <<" TOP CDT error status: "
                   << tcdt.read_reg("ofc1_err") << std::endl;
         std::cout <<" TOP CDT stop status: "
                   << tcdt.read_reg("ofc1_stop") << std::endl;
*/
         std::cout<<std::dec;
         std::cout <<" === ndelta trigger = " << tcdt.read_reg("ndelta")
                   << std::endl;
         std::cout <<" === # lv1 trigger = " << tcdt.read_reg("lv1_cnt")
                   << std::endl;
         std::cout <<" === # ext trigger = " << tcdt.read_reg("ext3_cnt")
                   << std::endl;
         std::cout <<" === # Loss due to 20-clock gate = " << tcdt.read_reg("lv1a_rej_cnt")
                   << std::endl;
         std::cout <<" === # Loss due to OFC-I busy = " << tcdt.read_reg("lv2_rej_cnt")
                   << std::endl; 
         std::cout <<" OFC-I #pileup = " << ofc1.read_reg("n_pileup") << std::endl;
         std::cout <<" OFC-I pending error = " << ofc1.read_reg("pending_err") << std::endl;
         std::cout <<" OFC-I busy = " << ofc1.read_reg("stop") << std::endl;
         std::cout <<" OFC-I package overflow = " << ofc1.read_reg("pkg_overflow") << std::endl;
         std::cout <<" OFC-I 00 #queue = " << (ofc1.read_word(0x28) & 0x3f) << std::endl;

         std::cout<<std::endl;
         std::cout<<std::endl;
         std::cout<<std::endl;
       }


       prelive = rlive;
       usleep(100* 1000);
       timer++;
   }

   return 0;

}

