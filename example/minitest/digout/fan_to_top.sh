#!/bin/bash

TOP_slot=3
FAN_slot=5
OFC_slot=7

input_id=4

vme="/root/vme7700"
e14vme="/misc/dev/executable/diagnosis/e14vme/bin/e14vme"

# switch to test mode for TOP CDT and FANOUT-II CDT
echo -e "switch to test mode."
${vme} write ${TOP_slot} 3c 1
${vme} write ${FAN_slot} 3c 1
${vme} write ${OFC_slot} 3c 1

# read version first
echo -e "version code TOP / FANOUT / OFC1"
${vme} read ${TOP_slot} 0
${vme} read ${FAN_slot} 0
${vme} read ${OFC_slot} 0

# 
echo -e "Before starting the test, count should be ffff."
${vme} write ${FAN_slot} 4 80000000 
${vme} write ${TOP_slot} 44 1
# addr 177 [15:0] result
# addr 2 [9:7] switch to read LVDS rx
${e14vme} write tcdt ${TOP_slot} "lvds_rx_err_cnt_sw" ${input_id}
${e14vme} read tcdt ${TOP_slot} "lvds_rx_err_cnt"
${vme} read ${TOP_slot} 5dc

# sending trigger
echo -e "Start consecutive numbers from OFC-1"
${vme} write ${FAN_slot} 44 1
# register #1 [31:30] = 1: consecutive integers for debugging
${vme} write ${FAN_slot} 4 40000000
sleep 2


# check numbers
echo -e "TOP CDT number of errors (should be 0) = "
echo -e "                         (max err = 0xfffd, no input = 0xffff) "
${e14vme} write tcdt ${TOP_slot} "lvds_rx_err_cnt_sw" ${input_id}
${e14vme} read tcdt ${TOP_slot} "lvds_rx_err_cnt"

# switch back to standard error output signal
${vme} write ${FAN_slot} 4 0
