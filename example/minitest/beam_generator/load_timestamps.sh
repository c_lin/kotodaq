#!/bin/bash

FILENAME="./hex/timestamps_50000.txt"
#FILENAME="timestamp_test.txt"
LINES=$(cat $FILENAME)

echo $LINES

slot_number=12
addr1=C
addr2=8
interval=1


address_val=0
/root/vme7700 write ${slot_number} 3c 1

/root/vme7700 write ${slot_number} 1c 3b9aca00
/root/vme7700 write ${slot_number} 10 10000000
for LINE in $LINES
do
        #echo ${LINE}
	/root/vme7700 write ${slot_number} ${addr1} ${address_val}
	/root/vme7700 write ${slot_number} ${addr2} ${LINE}
	address_val=`printf "0x%X\n" $(($address_val + $interval))`
	echo $address_val
	
done	

/root/vme7700 write ${slot_number} 3C 0 


