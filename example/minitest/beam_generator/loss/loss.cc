#include <cstdio>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <bitset>

#include "Register.h"
#include "VMEController.h"
#include "TopCDTManager.h"
#include "FADCManager.h"

int main()
{
   const int nspill = 100;

   std::cout<<" initialization... " << std::endl;
   TopCDTManager  tcdt(3);
   VMEController  f2cdt(5,"f2cdt");
   VMEController  ofc1(7,"ofc1");
   FADCManager    fadc1(9,0);      
   FADCManager    fadc2(10,0);      
   FADCManager    fadc3(11,0);      

   std::cout<<" TOP       CDT version: " << std::hex << tcdt.GetVersion()  << std::endl;
   std::cout<<" FANOUT-II CDT version: " << std::hex << f2cdt.GetVersion() << std::endl;
   std::cout<<" OFC-I         version: " << std::hex << ofc1.GetVersion()  << std::endl;
   std::cout<<" FADC1         version: " << std::dec << fadc1.GetVersion() << std::endl;
   std::cout<<" FADC2         version: " << std::dec << fadc2.GetVersion() << std::endl;
   std::cout<<" FADC3         version: " << std::dec << fadc3.GetVersion() << std::endl;

   // OFC-I setup
   ofc1.write_reg("test_mode",1);
   ofc1.write_reg("reset",1);
   ofc1.write_reg("ena_olrx",0x7);
   ofc1.write_reg("test_mode",0x1);
   ofc1.write_reg("input_mask",0xfff8);

   // FANOUT CDT setup
   f2cdt.write_reg("lvds_tx_sw",0x0);
   f2cdt.write_reg("err_ena",0x1);

   // TOP CDT setup
   tcdt.SetStandaloneMode(true);
   tcdt.SetTestMode(true);

   tcdt.write_reg("live_mode",0);

   tcdt.write_reg("ena_ofc1_err",0x1);

   /////
   tcdt.write_reg("delta_veto_ptn", 0x0);
   tcdt.write_reg("delta_et_thr",0x1);
   tcdt.write_reg("et_thr",0x1);
   
   /// disable veto inputs (> v40.00.06)
   tcdt.write_reg("fiber_in_sw",0x6);

   tcdt.write_reg("reset",1);
   tcdt.write_reg("nspill",nspill);

   tcdt.write_reg("start_delay",2);

   tcdt.write_reg("internal_live_period", 50 ); // ~ 4 seconds
   tcdt.write_reg("live_length", 100); 

   tcdt.write_reg("bypass_tlk_err",1);
   tcdt.write_reg("bypass_dc_err",1);
   tcdt.write_reg("bypass_local_align",1);
   tcdt.write_reg("bypass_global_align",1);

   tcdt.write_reg("ext3_ena",1);
   tcdt.write_reg("ext3_spill_on",1);
   tcdt.write_reg("ext3_spill_off",1);

   //tcdt.SetInternalClock(0xfffff);
   tcdt.SetInternalClock(0);
 
   fadc1.Init();
   fadc1.SetAnalysisMethod(e14::k_ET);
   fadc1.SetDaisyChain(3);

   fadc2.Init();
   fadc3.Init();

   std::cout<<" operation mode. " << std::endl;
   ofc1.write_reg("test_mode",0);
   tcdt.SetTestMode(false);

   std::cout<<std::dec;

   int spill_cnt = 0;
   bool prelive = false;

   int timer = 0;
   double sum_sent = 0; 
   double sum_rej= 0;
   double running_loss = 0.;
   while( spill_cnt < nspill )
   {
      bool rlive  = tcdt.read_reg("live");
      double sent;
      double rej;
      if( rlive == 1 && prelive == 0 ){
         timer = 0;
      }

      if( timer==10 ){
         spill_cnt =  tcdt.read_reg("spill_count") ;
         std::cout <<" spill no. = " << spill_cnt << std::endl;
         std::cout <<" === spill cycle length = " << tcdt.read_reg("spill_cycle_length") 
                   * 0.067 << " (sec)" << std::endl;    
         std::cout <<" delta ET pattern: " << std::hex 
                   << tcdt.read_reg("delta_et_raw") << std::endl;
         //std::cout <<" delta ET delay time: " << std::dec 
         //          << tcdt.read_reg("delay_et_delta_ptn") << std::endl;
         std::cout <<" OFC-I TLK err result : " << std::hex
                   << ofc1.read_reg("tlk_err") << std::endl;
//         std::cout <<" OFC-I energy word err? : " << std::hex
//                   << ofc1.read_reg("eneword_err") << std::endl;
         std::cout <<" OFC-I event no err  : " << std::hex
                   << ofc1.read_reg("evtno_err") << std::endl;
         std::cout <<" OFC-I spill no result : " << std::hex
                   << ofc1.read_reg("spillno_err") << std::endl;
         std::cout <<" FANOUT2 CDT error status : " 
                   << f2cdt.read_reg("err_status") << std::endl;
         std::cout <<" FANOUT2 CDT stop status : " 
                   << f2cdt.read_reg("stop_status") << std::endl;
         std::cout <<" TOP CDT error status: " 
                   << tcdt.read_reg("ofc1_err") << std::endl;
         std::cout <<" TOP CDT stop status: "
                   << tcdt.read_reg("ofc1_stop") << std::endl;

       }

       std::cout<<std::dec;

       if( rlive == 0 && prelive == 1 ){
         std::cout <<" === ndelta trigger = " << tcdt.read_reg("ndelta")
                   << std::endl;
         std::cout <<" === # lv1 trigger = " << tcdt.read_reg("lv1_cnt")
                   << std::endl;
         std::cout <<" === # ext trigger = " << tcdt.read_reg("ext3_cnt")
                   << std::endl;
         std::cout <<" === # Loss due to 20-clock gate = " << tcdt.read_reg("lv1a_rej_cnt")
                   << std::endl;
         std::cout <<" === # Loss due to OFC-I busy = " << tcdt.read_reg("lv2_rej_cnt")
                   <<  std::endl;
         std::cout<<" ==== # pile up in OFC-I = " << ofc1.read_reg("n_pileup") << std::endl; 
	 sent = (tcdt.read_reg("ext3_cnt") - tcdt.read_reg("lv1a_rej_cnt"));
	 rej = tcdt.read_reg("lv2_rej_cnt");
	 //if(sent*0.85 < rej){
	//	sent = 0;
	//	rej = 0;
	//	}
	 sum_sent = sum_sent + sent;
	 sum_rej = sum_rej + rej;
	 running_loss = 100*sum_rej/sum_sent;
	 std::cout << " === # Sum sent = " << sum_sent << std::endl;
	 std::cout << " === # Sum rej = " << sum_rej << std::endl;
	 std::cout << " === # Loss Rate = " << running_loss << "%" << std::endl;

         std::cout<<std::endl;
         std::cout<<std::endl;
         std::cout<<std::endl;
       }


       prelive = rlive;
       usleep(100* 1000);
       timer++;
   }

   return 0;

}

