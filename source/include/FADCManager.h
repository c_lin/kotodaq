/*!
 * @file       FADCManager.h
 * @brief      The DAQ functions for the ADC boards via the VME commands.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */
#ifndef FADCMANAGER_H
#define FADCMANAGER_H

#include <iostream>
#include <cstdio>
#include <string>
#include <fstream>
#include <vector>
#include <utility>

#include "Register.h"
#include "E14DAQParameter.h"
#include "E14DAQFunction.h"
#include "VMEController.h"

class FADCManager : public VMEController 
{

 public:
   //! ADC board type.
   enum E_ADCBoardType { k_125MHzADC=0,    //!< 125-MHz ADC board.
                         k_500MHzADC,      //!< 500-MHz ADC board.
                         k_TrigTagADC,     //!< Trigger tag ADC board.
                         k_UnknownADC      //!< Unknown ADC board.
                       };

 public:
   FADCManager( int slotId, int id = -1 );
    ~FADCManager();

   void Init( void );
   void ResetSpill();
   void ResetTrigCount();

   void SetId( int id ){ m_id = id; }

   inline int            GetId()        const { return m_id;        }
   inline E_ADCBoardType GetBoardType() const { return m_boardType; }

   void SetAnalysisMethod( e14::E_AnalysisMethod method, 
                           e14::E_HitCountFlagState flag = e14::k_NoneHitCountFlag );

   void SetDBFileDir( std::string const& fileDir ){ m_dbFileDir = fileDir; }

   bool SetDaisyChain( int nlack );
   bool SetSinglePeakThreshold( std::vector<int> const& pvec );
   bool SetDualPeakThreshold( std::vector<int> const& p0vec, 
                              std::vector<int> const& p1vec );
   bool SetDualVetoBit( e14::E_HitCountFlagState flag = e14::k_NoneHitCountFlag );
   bool SetDualVetoThreshold( int thr0, int thr1 );

   void SetSingleThr( bool isSingleThr = true );
   void SetQlinMode( bool enable = true );


   bool SetETLinearFunc( std::vector<int> const& p0vec, 
                         std::vector<int> const& p1vec );
   bool SetTimingWindow( std::pair<int, int> const& tpair );
   bool SetTimingWindow( int tleft, int tright );
   void SetTimingWindowDelay( int delay, e14::E_AnalysisMethod method );
   void SetHitCountFlag( e14::E_HitCountFlagState flag );
   bool SetChannelDelay( std::vector<int> const& delayvec );
   bool SetChannelMasking( std::vector<bool> const& maskvec );

   void EnableDSMode( void );
   void DisableDSMode( void );
   void CollectPedData( void );
   void RunDSCaptureForPed( void );

   void SetDigoutClockPhase( int phase );


   void SetPeakScan( int init_delay );
   void RunPeakScan( void );

   void RunDBCapture( int runID );
   void RunDBCaptureSpillBase( int runID, int SpillNo );

   static const int s_DCErrMaxTime = 50;
   static const int s_nChannel125MHzADC = 16;
   static const int s_nChannel500MHzADC = 4 ;
   static const int s_MaxCount125MHzADC = 0x3fff;
   static const int s_MaxCount500MHzADC = 0xfff;
   static const int s_nDaisyChain = 4;
   static const int s_nDaisyChainMember = 4;


 private:
   int m_id;
   int m_runId;
   E_ADCBoardType m_boardType; 

   void SetBoardType( void );
   bool Set1stPeakThresholdFor125MHzADC( const std::vector<int> &pvec );
   bool Set2ndPeakThresholdFor125MHzADC( const std::vector<int> &pvec );
   bool SetDualPeakThresholdFor500MHzADC( const std::vector<int> &p0vec, 
                                          const std::vector<int> &p1vec );
   bool SetDualVetoBitFor125MHzADC( int vb0, int vb1, e14::E_HitCountFlagState flag );
   bool SetDualVetoBitFor500MHzADC( int vb0, int vb1 );

   bool SetChannelDelayFor125MHzADC( std::vector<int> const& delayvec );
   bool SetChannelDelayFor500MHzADC( std::vector<int> const& delayvec );

   bool SetChannelMaskingFor125MHzADC( std::vector<bool> const& maskvec );
   bool SetChannelMaskingFor500MHzADC( std::vector<bool> const& maskvec );

   bool OpenRawOutput( std::ofstream &ofile, char const* name );
   bool FillDSBuffer( void );
   void WriteRawFile( std::ofstream& ofile );

   bool CheckId( void ) const;

 private:
   std::string m_dbFileDir;

};


#endif /* FADCManager.h guard */
