#include <iostream>
#include <sstream>
#include <vector>
#include <utility>
#include <fstream>
#include "E14DAQFunction.h"
#include "E14DAQParameter.h"

class SettingFile
{
public:
   SettingFile( int crateId );
   ~SettingFile();

   void Init( void );
   bool LoadDetMapFile (void);
   bool ExportSettingFile( std::ofstream &ofile, char const* addr );
   bool SettingFile::LoadBitAddressFile( void );

public:
   std::vector<std::vector<std::string> > m_detvec;
   std::vector<std::vector<int> > m_modvec;

private:
   const int m_crateId;
   bool      m_isError;


};

