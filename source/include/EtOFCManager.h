/*!
 * @file       EtOFCManager.h
 * @brief      The board communication interface of ET OFC.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */
#ifndef ETOFCMANAGER_H
#define ETOFCMANAGER_H

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>

#include "Register.h"
#include "E14DAQParameter.h"
#include "E14DAQFunction.h"
#include "VMEController.h"

class EtOFCManager : public VMEController 
{
 public:
   EtOFCManager( int slotId = s_defaultSlotId );
    ~EtOFCManager();

   void Init( void );

   bool SetVetoBitShift( std::vector<int> &bvec, int inputId );
   bool SetVetoBitMasking( std::vector<bool> &bvec, int inputId );
   bool SetVetoBitFromExternalFile( char const* type = "" );

   static const int s_defaultSlotId = 11;

   //
   void EnableMollyFile( bool enable = true ){ m_enableMollyFile = enable; }
   void SetnWordsPerSpill( int nwords ){ m_nwords_per_spill = nwords; }
   void RunMollyFile( int runId, int spill );

 private:
   bool  m_enableMollyFile;
   int   m_init_word;
   int   m_nwords_per_spill;
   int   m_spill;

   unsigned long*  m_data;

};

#endif /* EtOFCManager.h guard */
