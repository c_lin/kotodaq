/*!
 * @file       TopCDTManager.h
 * @brief      The board communication interface of TOP CDT.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */

#ifndef TOPCDTMANAGER_H
#define TOPCDTMANAGER_H

#include <iostream>
#include <cstdio>
#include <string>
#include <fstream>

#include "Register.h"
#include "E14DAQParameter.h"
#include "VMEController.h"


class TopCDTManager : public VMEController 
{
 public:

   TopCDTManager( int slotId = s_defaultSlotId );
    ~TopCDTManager();

   void Init( void );

   void SetStandaloneMode( bool isStandalone = true );
   void SetInternalClock( int user_interval );
   // void SetErrorMessageLevel( int level = 0 );

   bool SetTrigType( char const* type );
   bool SetExtTrigType( char const* type );

   bool SetTrigType( char const* type , int typeno );
   bool SetExtTrigType( char const* type, int typeno );

   void UpdateErr( void );
   void UpdateCounter( void );

   bool SaveErr( int runId, int spillId );
   bool SaveCounter( int runId, int spillId );

   void PrintErr( void );
   void PrintCounter( void );

   inline bool GetTLKErr() const { return m_isTLKErr; }
   inline bool GetDCErr() const { return m_isDCErr; }

   //! The default slot number for TOP CDT.
   static const int s_defaultSlotId = 2;

   //! The number of internal trigger types.
   static const int s_nInternalTrigType = 8;

   //! The number of external trigger types.
   static const int s_nExternalTrigType = 4;

   //! The number of external data counter (TMON, SEC).
   static const int s_nExternalDataCounter = 2;

   //! The number of counters for each #cluster..
   static const int s_nClusterCounter = 10;

   //! The number of counters for vetoes (= #veto bits).
   static const int s_nVetoCounter = 32;

 private:
   bool   m_isTLKErr;
   bool   m_isDCErr;
   bool   m_isLocalAlignErr;
   bool   m_isGlobalAlignErr;
   bool   m_isClusDeltaErr;
   bool   m_isClusIdErr;
   bool   m_isClusTimeErr;

   int    m_EtAlignPtnTime;
   int    m_Veto0AlignPtnTime;
   int    m_Veto1AlignPtnTime;
   int    m_EtDeltaPtnTime;
   int    m_Veto0DeltaPtnTime;
   int    m_Veto1DeltaPtnTime;
   int    m_DelayClusTime;
   int    m_EarlyLv1Time;
   int    m_EtDeltaLv1aTime;
   int    m_VetoDeltaLv1aTime;

   unsigned long  m_EtDeltaPtn;
   unsigned long  m_VetoDeltaPtn;

   int    m_nDelta;
   int    m_lv1aRawCnt;
   int    m_lv1aCnt;
   int    m_lv1aRejCnt;
   int    m_lv1Cnt;
   int    m_lv1PsCnt;
   int    m_lv2RejCnt;

   int    m_scaledClusCnt[s_nClusterCounter];
   int    m_typeLv1aScaledCnt[s_nInternalTrigType];
   int    m_typeLv1bScaledCnt[s_nInternalTrigType];
   int    m_vetoCnt[s_nVetoCounter];
   int    m_extCnt[s_nExternalTrigType];
   int    m_extData[s_nExternalDataCounter];
   int    m_etCnt;

   //bool   m_isEtOFCTLKErr;
   //bool   m_gotEtOFCTLKErr;
};

#endif /* TopCDTManager.h guard */
