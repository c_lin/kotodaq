/*!
 * @file       VetoFanoutCDTManager.h
 * @brief      The board communication interface of VETO FANOUT CDT.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */
#ifndef VETOFANOUTCDTMANAGER_H
#define VETOFANOUTCDTMANAGER_H

#include <iostream>

#include "Register.h"
#include "E14DAQParameter.h"
#include "VMEController.h"

class VetoFanoutCDTManager : public VMEController 
{
 public:
   VetoFanoutCDTManager( int slotId = s_defaultSlotId );
    ~VetoFanoutCDTManager();

   void Init( void );

   static const int s_defaultSlotId = 8;
};

#endif
