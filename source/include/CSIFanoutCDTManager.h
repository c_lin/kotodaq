/*!
 * @file       CSIFanoutCDTManager.h
 * @brief      The board communication interface of CSI FANOUT CDT.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */

#ifndef CSIFANOUTCDTMANAGER_H
#define CSIFANOUTCDTMANAGER_H

#include <iostream>

#include "Register.h"
#include "E14DAQParameter.h"
#include "VMEController.h"

class CSIFanoutCDTManager : public VMEController 
{
 public:
   CSIFanoutCDTManager( int slotId = s_defaultSlotId );
    ~CSIFanoutCDTManager();

   void Init( void );
 
   static const int s_defaultSlotId = 5;
};

#endif /* CSIFanoutCDTManager.h guard */
