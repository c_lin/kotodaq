/*!
 * @file   VMEController.h
 * @brief  The interface of the KOTO boards (ADC, CDT, OFC) on the VME controller.
 *
 * This class provides an understandable environment for the register-reading/writing of 
 * a KOTO board.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb
 * @copyright  KOTO (E14)
 */
#ifndef VMECONTROLLER_H
#define VMECONTROLLER_H

#include <iostream>
#include <cstdio>
#include <iomanip>
#include <string>
#include <fstream>

#include <vme/vme.h>
#include <vme/vme_api.h>

#include "Register.h"

#define ADDRESS_MODIFIER  VME_A32SD
#define ADDRESS_MODIFIER_BLOCK VME_A32SB

class VMEController
{

 public:
    VMEController( int slotId );
    VMEController( int slotId, std::string bdname );
    virtual ~VMEController();

 public:
   inline int         GetCrateId()        const { return m_crateId; } 
   inline int         GetSlotId()         const { return m_slotId;  }
   inline std::string GetBoardName()      const { return m_bdname;  }
   inline bool        IsBoardRegistered() const { return m_isBoardRegistered; }
   inline bool        IsCheckWriteFlag()  const { return m_isCheckWriteFlag; }
   unsigned long      GetVersion( void );

   inline void        SetCrateId( int crateId ) { m_crateId = crateId; }
   inline void        SetRunId( int runId )     { m_runId = runId;     }
   inline void        SetCheckWriteFlag( bool isCheckWriteFlag )
                                                { m_isCheckWriteFlag = isCheckWriteFlag; }

   void Init( void );  
   void InitAllRegister( void );
   void PrintAllRegister( void );
   void SetTestMode( bool isTestMode = true );
   void Reset( void );

   unsigned long read_word ( unsigned long addr );
   bool          write_word( unsigned long addr, 
                             unsigned long value);

   unsigned long read_reg ( struct reg::control_reg const* ctrl_reg, 
                            const char name[32] );
   bool          write_reg( struct reg::control_reg const* ctrl_reg, 
                            const char name[32], 
                            unsigned long new_value );

   unsigned long read_reg ( const char name[32] );
   bool          write_reg( const char name[32], 
                            unsigned long new_val );

   bool read_blt( unsigned long addr, 
                  unsigned long *buffer, 
                  int           nwords );

 protected:
   int m_crateId;
   int m_slotId;
   int m_runId;
   std::string m_bdname;

   bool               SaveRegisterErr( char const* reg_name , unsigned long new_value );
   
 private:
   struct reg::control_reg const *m_ctrl_reg;
   bool m_isBoardRegistered;
   bool m_isCheckWriteFlag;

   static const int s_nByte = 0x01;
   static const int s_nWord = 32; 
   static const unsigned long s_defaultErrVal = 0xffffffff;
   static const int s_maxNreadTrial = 3;
   static const int s_maxNwriteTrial = 10;
 
};

#endif /* VMEController.h guard */
