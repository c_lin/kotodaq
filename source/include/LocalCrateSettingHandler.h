/*!
 * @file   LocalCrateSettingHandler.h
 * @brief  A class handling all of the setting parameters for each FADC in a locla crate.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb
 * @copyright  KOTO (E14)
 */

#ifndef LOCALCRATESETTINGHANDLER_H
#define LOCALCRATESETTINGHANDKER_H

#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <utility>

#include "E14DAQParameter.h"
#include "E14DAQFunction.h"

class LocalCrateSettingHandler
{
 public:
   LocalCrateSettingHandler( int crateId );
   ~LocalCrateSettingHandler();

   void Init( void );
   inline bool IsError( void ) const { return m_isError; }

   inline int GetCrateId( void ) const { return m_crateId; }
   inline int GetnFADC( void ) const { return m_slotvec.size(); }
   inline int GetSlotId( int fadcId ) const;
   inline e14::E_AnalysisMethod GetAnalysisMethod( void ) const { return m_analysisMethod; }

   void SetAnalysisMethod( e14::E_AnalysisMethod method ){ m_analysisMethod = method; }

   bool LoadPeakThresholdFromExternalFile( char const* type );
   bool LoadETLinearFuncFromExternalFile( void ); 
   bool LoadChannelDelayFromExternalFile( void );
   bool LoadChannelMaskingFromExternalFile( char const* type );
   bool LoadTimingWindowFromExternalFile( char const* type );
   bool LoadDaisyChainFromExternalFile( void );
   bool LoadHitCountSettingFromExternalFile( void );
   bool LoadHitCountRangeFromExternalFile( void );
   bool LoadHitCountFlagFromExternalFile( void );

   std::vector<int>  const& Get1stPeakThresholdVector( int fadcId ) const;
   std::vector<int>  const& Get2ndPeakThresholdVector( int fadcId ) const;
   std::vector<int>  const& GetETLinearFuncP0Vector( int fadcId ) const;
   std::vector<int>  const& GetETLinearFuncP1Vector( int fadcId ) const;
   std::vector<int>  const& GetChannelDelayVector( int fadcId ) const;
   std::vector<bool> const& GetChannelMaskingVector( int fadcId ) const;
   std::pair<int, int> const& GetTimingWindowPair( int fadcId ) const;
   int GetnLackOfDaisyChain( int fadcId ) const;
   std::vector<std::pair<int, int> > const& GetHitCountRangePairVector() const { return m_nhitvec; }
   e14::E_HitCountFlagState GetHitCountFlag( int fadcId ) const;

 private:
   const int m_crateId;
   bool      m_isError;
   e14::E_AnalysisMethod m_analysisMethod;

   std::vector<int> m_slotvec;
   std::vector<std::vector<int> >        m_pthr0vec;
   std::vector<std::vector<int> >        m_pthr1vec;
   std::vector<std::vector<int> >        m_etp0vec;
   std::vector<std::vector<int> >        m_etp1vec;
   std::vector<std::vector<int> >        m_delayvec;
   std::vector<std::vector<bool> >       m_maskvec;
   std::vector<int>                      m_lackvec;
   std::vector<std::pair<int, int> >     m_tvec;
   std::vector<std::pair<int, int> >     m_nhitvec;
   std::vector<e14::E_HitCountFlagState> m_hitflagvec;

   std::vector<int> m_null_intvec;
   std::vector<bool> m_null_boolvec;
   std::pair<int, int> m_err_pair;

   bool LoadSlot( void );

   bool LoadSinglePeakThresholdFromExternalFile( std::ifstream& ifile );
   bool LoadDualPeakThresholdFromExternalFile( std::ifstream& ifile ); 

   bool CheckFADCIdValidity( int fadcId, char const* label = "" ) const;
   bool CheckSequence( unsigned int size, int id, char const* label = "" ) const;

};

//
//! @brief The slot number of this board.
//! @param[in] fadcId is the FADC ID counted from 0 in this crate.
//! @return The slot number of this crate. -1 is returned if invalid.
//
int LocalCrateSettingHandler::GetSlotId( int fadcId ) const
{
   return ( fadcId<0 || fadcId>=m_slotvec.size() ) ? -1 : m_slotvec[fadcId];
}

#endif
