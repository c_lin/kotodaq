/*!
 * @file       E14DAQFunction.h
 * @brief      The functions used for DAQ settings.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 */
#ifndef E14DAQFUNCTION_H
#define E14DAQFUNCTION_H

#include <iostream>
#include <fstream>

#include "E14DAQParameter.h"

class E14DAQFunction
{
 public:
   E14DAQFunction();
   ~E14DAQFunction();

   static int  GetDaisyChainRank( int fadcId );
   static int  GetDaisyChainId( int fadcId );
   static int  GetFADCIdFromDaisyChainRank( int inId, int rank );   
   static bool LoadSettingFile( std::ifstream &ifile, char const* name );

};

#endif /* E14DAQFunction.h guard */
