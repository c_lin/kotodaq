/*!
 *  @file E14DAQSettings.h 
 *  This namespace containes All of the fundemental parameters  for KOTO board settings.
 *
 *  @author     C. Lin (jay) [ linchieh212@gmail.com ]
 *  @date       2020 Feb
 *  @version    v5
 *  @copyright  KOTO
 */
#ifndef E14DAQPARAMETER_H
#define E14DAQPARAMETER_H

#include <string>

/**
 *  @addtogroup Basic DAQ parameters
 *  @{
 */

namespace e14
{
   //! Hit count state for the multiplicity data transmission. 
   //! The hit summation is performed over the same type.
   enum E_HitCountFlagState { k_NoneHitCountFlag=0, //!< hit count is inactivated.
                              k_HitCountType1Flag,  //!< hit count type 1 is assigned.
                              k_HitCountType2Flag   //!< hit count type 2 is assigned.
                            };


   //! Analysis method for the board.
   enum E_AnalysisMethod{ k_VetoSingleThr=0,
                          k_VetoDualThrWithHitCount,
                          k_VetoDualThrWoHitCount,
                          k_ET,
                          k_UndefinedMethod
                        };

   //! Number of crates in use.
   const int k_nCrate = 18;

   //! Default analysis method for each crate;
   const E_AnalysisMethod k_CrateAnalysisMethod[ k_nCrate ] =
   { k_ET,                        k_ET,                        k_ET,              
     k_ET,                        k_ET,                        k_ET,                    
     k_ET,                        k_ET,                        k_ET,                    
     k_ET,                        k_ET,                        k_VetoDualThrWithHitCount, 
     k_VetoDualThrWoHitCount,     k_VetoDualThrWoHitCount,     k_VetoDualThrWoHitCount, 
     k_VetoSingleThr,             k_VetoSingleThr,             k_VetoDualThrWoHitCount
   };


   //! Default value of raw pulse pipeline delay readout at TOP CDT.
   const unsigned long k_DelayRaw = 370;  

   //! Default value of clustering pipeline delay readout at TOP CDT.
   const unsigned long k_DelayLv1bReq = 210;

   //! Default value of extra delay for Lv1 trigger issuing from TOP CDT.
   const unsigned long k_DelayLv1 = 44;

   //! Default value of the spill off time after live is on.
   const unsigned long k_SpillOffTime = 350000000;

   //! Number of Daisy Chains
   const int k_nDaisyChain = 4;

   //! Number of members in a Daisy Chain
   const int k_nDaisyChainMember = 4;

   //! Default ET OFC maximum time to receive the alignment pattern.
   const unsigned long k_EtOFCPtnRxMaxTime = 250;

   //! Default ET OFC maximum time to send out the error message.
   const unsigned long k_EtOFCErrMaxTime = 500;

   //! Number of bits of a word for O/L.
   const int k_nBitOfOLWord = 16;

   //! Number of bits to perform a shift.
   const int k_nBitOfShift = 4;

   //! Number of pairs from LOCAL CDT to ET OFC.
   const int k_nPairFromLocalCDT = 8;

   //! Number of ADCs in a crate.
   const int k_nFADC = 16;

   //! The maximum time for a LOCAL CDT to receive all of the alignment pattern from ADCs.
   const int k_LocalCDTAlignMaxTime   = 500;

   //! The maximum time for a LOCAL CDT to receive all of the TLK error results from ADCs.
   const unsigned long k_LocalCDTTLKErrMaxTime  = 11250000;

   //! The maximum time for a LOCAL CDT to receive all of the DC error results from ADCs.
   const unsigned long k_LocalCDTDCErrMaxTime   = 12500500;

   //! The number of channels in 125-MHz ADC.
   const int k_n125MHzChannel = 16;

   //! The number of channels in 500-MHz ADC.
   const int k_n500MHzChannel = 4;

   //! The delay of the veto bit window from digitized inputs.
   const int k_VetoBitWindowDelay = 1;

   //! The delay of the cluster bit window (driven by pre-lv1) from digitized inputs..
   //const int k_ClusterBitWindowDelay = 361;
   const int k_ClusterBitWindowDelay = 358; // shiomi 2020/5/25
   
   //! The delay of the bit map writting in the header for clustr bits.
   const int k_ClusterBitMapDelay = 256;
   

   //! The required additional delay for 125-MHz ADC to be algined with 500-MHz one.
   const int k_DelayOffsetFor125MHzADC = 4;

   //! The required additional delay for the single threshold method to be aligned with dual method.
   const int k_DelayOffsetForSingleThr = 21;

   //
   typedef enum { CC03=0, CC04, CC05, CC06,
		  CBAR,   FBAR, CSI,  BHPV, 
		  CV,     NCCIndi, NCCComm, NCCScin,
		  OEV,    LCV,  DCV,  newBHCV,
		  BHGC,   IB,   IBCV, MBCV, 
		  MPPC,   COSMIC, ExtTrig } DetectorID;

   const int nDet = 23;  // Total 21 detector in use; +2 (NCC => Individual Common Scintilator)

   const std::string DetName[nDet]={ /*0*/"CC03",  /*1*/"CC04", /*2*/"CC05",  /*3*/"CC06",
				/*4*/"CBAR",  /*5*/"FBAR", /*6*/"CSI",   /*7*/"BHPV",
				/*8*/"CV",    /*10*/"NCCIndi", /*10*/"NCCComm", /*11*/"NCCScin",
				/*12*/"OEV",  /*13*/"LCV", /*14*/"DCV",  /*15*/"newBHCV",
				/*16*/"BHGC", /*17*/"IB",  /*18*/"IBCV", /*19*/"MBCV",
				/*20*/"MPPC", /*21*/"COSMIC", /*22*/"ExtTrig" };

/////////////////////////////////////////////////
// Below are thresholds to identify noisy channels for various detectors
// If detector characteristics unknown, for 125MHz set SigThr=>4.0 EtaThr=>0.75, for 500MHz set SigThr=>2.0 EtaThr=>0.80
// If one detector would not be applied pedestal-suppression, then set SigThr=>0.0 EtaThr=>1.00 (e.g. newBHCV NCC-Scintilator)
// If one detector would not need noisy channels to be identified, then set SigThr=>999 EtaThr=>0.01 (e.g. CV) 
   const double PedSigThr[nDet]={ 4.0, 3.5, 3.5, 4.0, 
				  3.5, 4.0, 4.0, 2.0,
				  999, 3.0, 4.0, 0.0,
				  4.0, 3.5, 4.0, 0.0,
				  2.0, 2.0, 3.5, 3.0,
				  4.0, 4.0, 4.0 };
   const double PedEtaThr[nDet]={ 0.75, 0.70, 0.75, 0.65,
				  0.75, 0.70, 0.75, 0.75,
				  0.01, 0.80, 0.65, 1.00,
				  0.80, 0.75, 0.75, 1.00,
				  0.80, 0.80, 0.75, 0.75,
				  0.75, 0.75, 0.75 };
/////////////////////////////////////////////////

   const std::string setdir = "/DAQcodes/e14daq/current/setting/current";
   const std::string rctrl_db_dir = "/DAQcodes/e14daq/current/run_ctrl/db_mode/current";
   const std::string trigcnt_dir = "/raw_data/e14daq_data/trigcnt";
   const std::string errlog_dir = "/raw_data/e14daq_data/errlog";
   const std::string parlog_dir = "/raw_data/e14daq_data/parlog";
   const std::string config_dir = "/DAQcodes/e14daq/current/run_ctrl/collect_data/current/run_config";
   //const std::string mon_dir = "/raw_data/monitor_data";
   const std::string mon_dir = "/DAQcodes/e14daq/current/monitor";

   const int nOLbit = 16;

   const int nFADCs = 16;
   const int nCrates = 18;
   const int nChannels = 16;
   const int n500mhzChannels = 4;
   const int nDaisyChain = 4;
   const int nDaisyChainMember = 4;
   const int nTrigTypes = 8;
   const int nExtTrig   = 4;

   //// run81 settings ////
   const int et_masking = 0x0;
   const int veto_masking = 0xffb0;

   const int delay_raw       = 370;
   const int delay_lv1b_req  = 210;
   const int delay_lv1       = 44; 

   //// test for run81
   const int delay_cb     = 361;
   const int delay_cb_map = 256;

   /// run79 settings 
   // const int delay_cb     = 361;
   // const int delay_cb_map = 256;

   const int delay_vb     = 150;
   const int delay_vb_map = 256; // 2018.06.04: change from 463 -> 256

   //const int delay_vb
  
   /// run81, 01/30 test 
   const int delay_offset_125mhz = 4;  // VB delay has 4 clock offset btw 125 and 500 mhz
   const int veto_ptn_maxt = 100;
   const int et_ptn_maxt = 250;
   const int et_dc_maxt = 50;

   /*     
    *  crate 13, board 4  - 12 : NCC individuals, board 15 : MPPC
    *  crate 14, board 12      : Trigger Tag Board (MUST BE MASKED!!!)
    *  crate 15, board 0       : Special BHPV channel 40-41
    *            board 11 - 15 : MPPCs
    *  crate 16, board 12 - 15 : MPPCs
    *     */
   const int adc_input_mask[nCrates] =
   {   0x0000 , 0x0000 , 0x0000 , 0x0000 , 0x0000
     , 0x0000 , 0x0000 , 0x0000 , 0x0000 , 0x0000
     , 0x0000 , 0xf000 , 0x0000 , 0x9ff0 , 0x1000
     , 0xf801 , 0xf000 , 0x0000 };


   const bool isVetoCrate[nCrates] = 
   {   false , false , false , false , false 
     , false , false , false , false , false 
     , false , true  , true  , true  , true
     , true  , true  , true  };

   const bool is500mhzCrate[nCrates] =
   {   false , false , false , false , false
     , false , false , false , false , false
     , false , false , false , false , false
     , true  , true  , true  };

   const int local_tlk_err_maxt = 11250000;
   const int local_dc_err_maxt  = 12500500;

   const int fanout_tlk_err_maxt = 11875000;
   const int fanout_dc_err_maxt = 12500700;

   //const int local_et_thr = 196608; // ( 3 MeV << 16 )
   const int local_et_thr = 1; // ( 3 MeV << 16 )


};

// for doxygen documentation:
//! @}

#endif /* E14DAQParameter.h guard */
