/*!
 * @file   TrigCrateSettingHandler.h
 * @brief  .
 *
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb
 * @copyright  KOTO (E14)
 */

#ifndef TRIGCRATESETTINGHANDLER_H
#define TRIGCRATESETTINGHANDKER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

class TrigCrateSettingHandler
{
 public:
   TrigCrateSettingHandler();
   ~TrigCrateSettingHandler();

   void Init( void );

   void LoadTrigSetting( std::string type );

 private:
   int m_crateId;
   std::string m_setdir;

   std::vector<int> m_slotvec;

   void LoadSlot( void );

};

#endif
