/*!
 * @file       ClusOFCManager.h
 * @brief      The board communication interface of CLUSTERING OFC.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */
#ifndef CLUSOFCMANAGER_H
#define CLUSOFCMANAGER_H

#include <iostream>

#include "Register.h"
#include "VMEController.h"

class ClusOFCManager : public VMEController 
{
 public:
   ClusOFCManager( int slotId = s_defaultSlotId );
    ~ClusOFCManager();

   void Init( void );

   static const int s_defaultSlotId = 14;
};

#endif /* ClusOFCManager.h guard */
