/*!
 * @file       LocalCDTManager.h
 * @brief      The board communication interface of LOCAL CDT.
 *
 * @author     C. Lin (jay) [ linchieh212@gmail.com ]
 * @version    v5
 * @date       2020 Feb.
 * @copyright  KOTO (e14)
 *
 */
#ifndef LOCALCDTMANAGER_H
#define LOCALCDTMANAGER_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

#include "Register.h"
#include "E14DAQParameter.h"
#include "E14DAQFunction.h"
#include "VMEController.h"

class LocalCDTManager : public VMEController 
{
 public:
   LocalCDTManager( int slotId = s_defaultSlotId );
    ~LocalCDTManager();

   void Init( void );
   void SetAnalysisMethod( e14::E_AnalysisMethod method );

   void SetDaisyChain( void );
   bool SetSingleVetoBitShift( std::vector<int> const& svec );
   bool SetSingleVetoBitMasking( std::vector<bool> const& mvec );
   bool SetDualVetoBitShift( int inputId, std::vector<int> const& svec );
   bool SetDualVetoBitMasking( int inputId, std::vector<bool> const& mvec );
   bool SetHitCountBitShift( int resultId, int type1_shift, int type2_shift );
   bool SetHitCountRange( int resultId, int min_count, int max_count );

   bool SetSingleVetoBitFromExternalFile( int crateId, char const* type = "" );
   bool SetDualVetoBitFromExternalFile( int crateId, char const* type = "" );
   bool SetHitCountBitFromExternalFile( int crateId, char const* type = "" );
   bool SetHitCountRangeFromExternalFile( int crateId );

   void SetDiginMasking( unsigned long mask_val );
   bool SetDiginMaskingFromDaisyChainFile( int crateId );
 
   static const int  s_defaultSlotId  = 1;

 private:
   void SetDefaultBitMasking( void );
   void SetDefaultHitCountRange( void );
};

#endif
