#include "EtOFCManager.h"

//
//! @brief Default constructor.
//! @param[in] slotId is the slot number.
//
EtOFCManager::EtOFCManager( int slotId ) 
   : VMEController( slotId, "eofc" )
{
   Init();

   // molly files
   m_nwords_per_spill = 6000;
   m_data = new unsigned long [65536];

}

//
//! @brief Default destructor.
//
EtOFCManager::~EtOFCManager()
{
   ;
}

//
//! @brief Initialization of this board with default values.
//
void EtOFCManager::Init( void )
{
   SetTestMode(0x1);
   Reset();

   write_reg("ena_lvds_cnt",0x0);

   write_reg("et_ptn_rx", 0xfefe);
   write_reg("et_maxt", e14::k_EtOFCPtnRxMaxTime);
   write_reg("et_masking", 0xf800 );

   write_reg("veto_ptn_rx", 0xfefe);
   write_reg("veto_maxt", e14::k_EtOFCPtnRxMaxTime);
   write_reg("veto_masking", 0xff80 );
 
   write_reg("err_maxt", e14::k_EtOFCErrMaxTime );
   write_reg("ena_olrx",0x3ffff);
}

//
//! @brief Setting of the bit shift at ET OFC.
//!
//! The input from LOCAL CDT should be 8 pairs of veto bits as following format:
//! ---> H1 G1 F1 E1 D1 C1 B1 A1 H0 G0 F0 E0 D0 C0 B0 A0
//! where A, B, .., H stand for the 8 detector and 0 and 1 stand for the dual threshold results.
//!
//! From the ET OFC to TOP CDT, there are two optical links (two 16-bit words). The bit shift 
//! operation acts on two results simultaneously and separate the results into two optical outputs.
//! 
//! For instance,
//! If assigning the first bit (A) with shift 2, the output will be
//! (optical output-0) - - - - - - A0 - -
//! (optical ouptut-1) - - - - - - A1 - -
//! 
//! If the same shift is assigned for different detectors, the OR is operated.
//!
//! @param[in] bvec is the integer vector containing the amount of shift for each bit. 
//!            The vector size should be exact 8 and the valid shift is 0-15.
//! @param[in] inputId is the input ID of veto from LOCAL CDT (range: 0-6).              
//! @return The register can be correctly written or not.
//
bool EtOFCManager::SetVetoBitShift( std::vector<int> &bvec, int inputId )
{
   if( inputId<0 || inputId>6 ){
      std::cout<<"Error in <EtOFCManager::SetVetoBitShift>"
               <<" invalid input ID: " << inputId <<" (range 0-6)." << std::endl;
      return false;
   }

   if( bvec.size()!=e14::k_nPairFromLocalCDT ){
      std::cout<<"Error in <EtOFCManager::SetVetoBitShift>"
               <<" the size of bvec should be " << e14::k_nPairFromLocalCDT << std::endl;
      return false;
   }

   unsigned long val = 0;
   for( int ibit=0; ibit<e14::k_nPairFromLocalCDT; ++ibit )
   {
      if( bvec[ibit]<0 || bvec[ibit]>15 ){
         std::cout<<"Error in <EtOFCManager::SetVetoBitShift>"
                  <<" invalid shift in bvec: " << bvec[ibit] << std::endl;
         return false;
      }
      val |= ((bvec[ibit]) << (ibit*4) ); 
   }

   char reg_name[32];
   sprintf( reg_name, "bit_shift_%d", inputId);
   write_reg( reg_name, val );

   return true;
}

//
//! @brief Setting of bit masking of an input at ET OFC.
//! @param[in] bvec is the boolean vector to determine which bit should be masked.
//             The vector size should be exact 16 and the valid shift is 0-15.
//! @param[in] inputId is the input ID of veto from LOCAL CDT (range: 0-6).
//! @return The register can be correctly written or not.
//
bool EtOFCManager::SetVetoBitMasking( std::vector<bool> &bvec, int inputId )
{
   if( inputId<0 || inputId>6 ){
      std::cout<<"Error in <EtOFCManager::SetVetoBitMasking>"
               <<" invalid input ID: " << inputId <<" (range 0 - 5)." << std::endl;
      return false;
   }

   if( bvec.size()!=e14::k_nBitOfOLWord ){
      std::cout<<"Error in <EtOFCManager::SetVetoBitMasking>"
               <<" the size of bvec should be " << e14::k_nBitOfOLWord << std::endl;
      return false;
   }

   unsigned long val = 0;
   for( int ibit=0; ibit<e14::k_nBitOfOLWord; ++ibit )
   {
      if( bvec[ibit] ) val += ( 1 << ibit );
   }

   char reg_name[32];
   sprintf( reg_name, "bit_mask_%d", inputId);
   write_reg( reg_name, val );
   
   return true;
}

//
//! @brief Setting of both bit shift and masking from an external file.
//!
//! The file name should be Crate%d_BitMap.txt under DAQ_SETTING_DIR/BitMap/type.
//! Format:
//!     "input no"      "bit no."       "shift"         "mask0"		"mask1"
//!     0               0               0               0		0
//!     0               1               0               1		0
//!     ..
//!     0               7               0               0		?
//!     1               0               0               0		?
//!     1               1               0               0		?
//!     ..
//!     6               7               ?               ?		?
//!
//! The contents is required in the sequence of input and bit number. 
//! The expected number of rows is 8(bits) x 7(inputs) = 56
//!
//! @warning The content is required to be in sequence of input and bit number.
//
bool EtOFCManager::SetVetoBitFromExternalFile( char const* type )
{
   char ifname[100];
   sprintf( ifname, "BitMap/%s/EtOFC_BitMap.txt", type);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   int r_in, r_bit, r_shift;
   bool r_mask0, r_mask1;
   std::vector<int> svec;
   std::vector<bool> m0vec, m1vec;
   while( ifile >> r_in >> r_bit >> r_shift >> r_mask0 >> r_mask1 )
   {
      if( r_bit != svec.size() ){
         std::cout<<"Error in <EtOFCManager::SetVetoBitFromExternalFile>"
                  <<" the bit number is not in sequence." << std::endl;
         return false;
      }

      svec.push_back(r_shift);
      m0vec.push_back(r_mask0);
      m1vec.push_back(r_mask1);

      if( svec.size()==e14::k_nPairFromLocalCDT )
      {
         if( !SetVetoBitShift( svec, r_in ) ) return false;
         m0vec.insert( m0vec.end(), m1vec.begin(), m1vec.end() );
         if( !SetVetoBitMasking( m0vec, r_in ) ) return false;

         svec.clear();
         m0vec.clear();
         m1vec.clear();
      }
   } 

   return true;
}


//
void EtOFCManager::RunMollyFile( int runId, int spill )
{
   if( spill<0 ) return;

   const int nMaxWords = 65535;

   if( m_enableMollyFile ){
      m_runId = runId;
      m_spill = spill + 1;
      write_reg("rawfile_en",1);
      m_enableMollyFile = false;
      m_init_word = 0;
      return;
   }

   m_enableMollyFile = false;
   write_reg("rawfile_en",0);
   const unsigned long reg_addr = 0x900000;
   int ntrig = read_reg("rawfile_ntrig");
   std::cout<<" Molly file: n words = " << ntrig << std::endl;

   int  init_word = m_init_word;
   int  end_word = m_nwords_per_spill + m_init_word;
   bool isReadAll = false;

   if( end_word > ntrig || end_word > nMaxWords ){
      end_word = ( ntrig > nMaxWords) ? nMaxWords : ntrig;
      m_enableMollyFile = true;
      isReadAll = true;
   }

   for( int iword = init_word; iword < end_word; ++iword ){
       unsigned long addr = reg_addr + 4 * iword;
       m_data[iword] = read_word(addr);
   }

   if( isReadAll ){
      char fname[300];
      sprintf( fname, "/raw_data/e14daq_data/molly_file/run%d_spill%d.txt", 
                      m_runId, m_spill );
      std::ofstream ofile( fname );

      /// add at 06/15 for protection
      int nwords = (ntrig > nMaxWords) ? nMaxWords : ntrig;

      for( int iword=0; iword<nwords; ++iword ){
         ofile << m_data[iword] << std::endl;
      }

      ofile.close();
      for( int i=0; i<nMaxWords; ++i )
         m_data[i] = 0;

   }else{
      m_init_word = m_nwords_per_spill + m_init_word;
   }

}
