#include "E14DAQFunction.h"

//
//! @brief Default constructor.
//
E14DAQFunction::E14DAQFunction( void )
{
   ;
}

//
//! @brief Default destructor.
//
E14DAQFunction::~E14DAQFunction( void )
{
   ;
}

//
//! @brief Get the rank of Daisy Chain by FADC ID
//! @param[in] fadcId is the FADC ID in a crate counted from 0 to 15.
//
int E14DAQFunction::GetDaisyChainRank( int fadcId )
{
   return ( fadcId>=0 && fadcId<16 ) ?
          ( e14::k_nDaisyChainMember - (fadcId / e14::k_nDaisyChain) - 1 ) : -1 ;
} 

//
//! @brief Get the Daisy Chain input ID by FADC ID
//! @param[in] fadcId is the FADC ID in a crate counted from 0 to 15.
//
int E14DAQFunction::GetDaisyChainId( int fadcId )
{
   return ( fadcId>=0 && fadcId<16 ) ?
          ( fadcId % e14::k_nDaisyChain ) : -1 ;
}

//
//! @brief Get the FADC ID (0-15) from Daisy-Chain rank.
//! @param[in] dcId is the O/L input ID of LOCAL CDT. (0-3)
//! @param[in] rank is the rank of this board in a Daisy Chain.
//
int E14DAQFunction::GetFADCIdFromDaisyChainRank( int dcId, int rank )
{
   if( dcId<0 || dcId>=e14::k_nDaisyChain || rank<0 || rank>e14::k_nDaisyChainMember ){
      std::cout<<"Warning in <E14DAQFunction::GetFADCIdFromDaisyChainRank>"
               <<" DC input ID: " << dcId << " and rank: " << rank << std::endl;
   }
   return ( e14::k_nDaisyChainMember - rank - 1) * e14::k_nDaisyChain + dcId; 
}

//
//! @brief Load of the setting file.
//! @param[in] name is the file name under DAQ_SETTING_DIR.
//! @param[in] ifile is the std::ifstream object to be loaded.
//! @return The correctness of the file loading.
//
bool E14DAQFunction::LoadSettingFile( std::ifstream &ifile, char const* name )
{
   char const* setdir = std::getenv("DAQ_SETTING_DIR");
   if( setdir==NULL ){
      std::cout<<"Error in <E14DAQFunction::LoadSettingFile>"
               <<" DAQ_SETTING_DIR is not set." << std::endl;
      return false;
   }

   char ifname[100];
   sprintf(ifname, "%s/%s", setdir, name );
   ifile.open( ifname );
   if( ifile.fail() ){
      std::cout<<"Error in <E14DAQFunction::LoadSettingFile>"
               <<" fail to read file:. " << ifname << std::endl;
      return false;
   }

   return true; 
}
