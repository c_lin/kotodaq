#include "VMEController.h"

//
//! @brief The constructor initialized by slot number.
//! @param[in] slotId The slot ID for this board.
//
VMEController::VMEController( int slotId ) 
   : m_slotId( slotId )
{
   m_crateId = -1;
   Init();
}

//
//! @brief The constructor initialized by slot number and board name.
//! @param[in] slotId The slot number for this board.
//! @param[in] bdname The board name for the register list connection.
//! <table>
//! <caption id="multi_row">Board type options</caption>
//! <tr><th> Option     <th> Board name
//! <tr><td> adc        <td> FADC
//! <tr><td> tcdt	<td> Top CDT
//! <tr><td> fcdt	<td> Fan-out CDT
//! <tr><td> lcdt	<td> Local CDT
//! <tr><td> eofc	<td> ET OFC
//! <tf><td> cofc	<td> Clustering OFC
//! </table>
//
VMEController::VMEController( int slotId, std::string bdname ) 
   : m_slotId(slotId),
     m_bdname(bdname)
{
   Init();
   m_isBoardRegistered = true;

   if( m_bdname == "adc" )
      m_ctrl_reg = reg::adc_reg;
   else if( m_bdname == "tcdt" )
      m_ctrl_reg = reg::tcdt_reg;
   else if( m_bdname ==  "fcdt" )
      m_ctrl_reg = reg::fcdt_reg;
   else if( m_bdname == "lcdt" )
      m_ctrl_reg = reg::lcdt_reg;
   else if( m_bdname == "eofc" )
      m_ctrl_reg = reg::eofc_reg;
   else if( m_bdname == "cofc" )
      m_ctrl_reg = reg::cofc_reg;
   else if( m_bdname == "ofc1" )
      m_ctrl_reg = reg::ofc1_reg;
   else if( m_bdname == "f2cdt" )
      m_ctrl_reg = reg::f2cdt_reg;
   else
   { 
      std::cout<<" [Error] No such board ! " << std::endl;
      m_isBoardRegistered = false;
   }
   m_crateId = -1;

}

//
//! @brief Default destructor.
//
VMEController::~VMEController()
{
   ;
}

//
//
//
void VMEController::Init( void )
{
   SetCheckWriteFlag(false);
}

//
//! @brief Initialization of all registers for this board.
//
void VMEController::InitAllRegister( void )
{
   if( !IsBoardRegistered() ) return;

   int ireg = 0;
   while( strcmp(m_ctrl_reg[ireg].name,"END") != 0  )
   {
      if( strcmp(m_ctrl_reg[ireg].type,"rw") == 0 )
         write_reg(m_ctrl_reg[ireg].name,m_ctrl_reg[ireg].default_value);
      ++ireg;
   }   
}

//
//! @brief Print all register values for this board.
//
void VMEController::PrintAllRegister( void )
{
   if( !IsBoardRegistered() ) return;

   int ireg = 0;
   while( strcmp(m_ctrl_reg[ireg].name,"END") != 0  )
   {
      std::cout<< std::setw(4) << std::dec << ireg;
      std::cout<< std::setw(24) << m_ctrl_reg[ireg].name
               << std::setw(5) << "(" << m_ctrl_reg[ireg].type << ")" 
               << std::setw(8) << std::hex <<  read_reg(m_ctrl_reg[ireg].name);
      if( (ireg%2)==1 ) std::cout<<"\t";
      else              std::cout<<std::endl;
      ++ireg;
   }
}

//
//! @brief Switch the board to the test mode.
//! @warning ADC does not have test mode.
//
void VMEController::SetTestMode( bool isTestMode )
{
   if( GetBoardName()!="adc" ) 
      write_reg("test_mode", isTestMode); 
}

//
//! @brief Reset the board.
//! @warning The spill and trigger count in ADC needs to be reset by <FADCManager::ResetSpill> and
//!          <FADCManager::ResetTrigCount>
//
void VMEController::Reset()
{
   if( GetBoardName()!="adc" ){
      write_reg("reset", 0x1);
      write_reg("reset", 0x1);
   }
}

//
//! @brief Read a 32-bit word from VME backplane.
//! @param[in] user_addr The address of this word.
//! @return The 32-bit word.
//
unsigned long VMEController::read_word( unsigned long user_addr )
{
   unsigned long addr = m_slotId*(0x8000000); 
   if( GetBoardName()!="adc" ) 
      addr += user_addr;

   vme_bus_handle_t     bus_handle;
   vme_master_handle_t  window_handle;
   uint32_t *ptr, data_word;

   unsigned long val = (GetBoardName()=="adc") ? s_nWord * 4 : s_nByte;

   //
   // LOOP the reading process 3 times.
   // The KOTO VME readout seems to be unstable. It requires repeatably reading to make it correct. 
   // 3 was obtained empirically.
   //
   for(int iloop=0; iloop<3; ++iloop )
   {
      if (vme_init(&bus_handle)){
         perror("Error initializing the VMEbus");
         return s_defaultErrVal;
      }

      if (vme_master_window_create( bus_handle, 
                                    &window_handle,
                                    addr, 
                                    ADDRESS_MODIFIER, 
                                    val,
                                    VME_CTL_PWEN, 
                                    NULL)) 
      {
         perror("Error creating the window");
         vme_term(bus_handle);
         return s_defaultErrVal;
      }

      ptr = static_cast<uint32_t*>( vme_master_window_map( bus_handle, 
                                                           window_handle, 
                                                           0             ) );

      if (!ptr) {
         perror("Error mapping the window");
         vme_master_window_release(bus_handle, window_handle);
         vme_term(bus_handle);
         return s_defaultErrVal;
      }

      data_word = (GetBoardName()=="adc") ? *(ptr + user_addr) : *(ptr);

      if (vme_master_window_unmap(bus_handle, window_handle)){
         perror("Error unmapping the window");
         vme_master_window_release(bus_handle, window_handle);
         vme_term(bus_handle);
         return s_defaultErrVal;
      }

      if (vme_master_window_release(bus_handle, window_handle)) {
         perror("Error releasing the window");
         vme_term(bus_handle);
         return s_defaultErrVal;
      }

      if (vme_term(bus_handle)) {
         perror("Error terminating");
         return s_defaultErrVal;
      }

   } // END loop

   return data_word;
}

//
//! @brief Write a 32-bit word into a register.
//! @param[in] user_addr The address to be written.
//! @param[in] value     The 32-bit word to be written.
//! @return Correctness of the VME writing.
//
bool VMEController::write_word( unsigned long user_addr, 
                                unsigned long value )
{
   unsigned long addr = m_slotId*(0x8000000);
   if( GetBoardName()!="adc" )
      addr += user_addr;
   
   vme_bus_handle_t     bus_handle;
   vme_master_handle_t  window_handle;
   uint32_t *ptr;

   if (vme_init(&bus_handle)) {
      perror("Error initializing the VMEbus");
      return false;
   }

   unsigned long val = ( GetBoardName()=="adc") ? s_nWord * 4 : s_nByte;

   if (vme_master_window_create( bus_handle, 
                                 &window_handle,
                                 addr, 
                                 ADDRESS_MODIFIER, 
                                 val,
                                 VME_CTL_PWEN, 
                                 NULL)) 
   {
      perror("Error creating the window");
      vme_term(bus_handle);
      return s_defaultErrVal;
   }

   ptr = static_cast<uint32_t*>( vme_master_window_map( bus_handle, 
                                                         window_handle, 
                                                         0)            );

   if (!ptr){
      perror("Error mapping the window");
      vme_master_window_release(bus_handle, window_handle);
      vme_term(bus_handle);
      return s_defaultErrVal;
   }

   if( GetBoardName()=="adc" )
      *(ptr + user_addr) = value;
   else
      *(ptr) = value;

   if (vme_master_window_unmap(bus_handle, window_handle)) {
      perror("Error unmapping the window");
      vme_master_window_release(bus_handle, window_handle);
      vme_term(bus_handle);
      return s_defaultErrVal;
   }

   if (vme_master_window_release(bus_handle, window_handle)) {
      perror("Error releasing the window");
      vme_term(bus_handle);
      return s_defaultErrVal;
   }

   if (vme_term(bus_handle)) {
      perror("Error terminating");
      return s_defaultErrVal;
   }

   return 0;
}

//
//! @brief Read the value of a register.
//! @param[in] *ctrl_reg   The control register in struct.
//! @param[in] name        The name of the register.
//! @return The register value.
//
unsigned long VMEController::read_reg( struct reg::control_reg const *ctrl_reg, 
                                       const char name[32] ) 
{
   unsigned long       data_word, addr;
   int                 i;

   for(i=0;i<reg::k_MaxNreg;i++)                                                    
   {
      if(strcmp(name,ctrl_reg[i].name)==0)break;
      if(strcmp(ctrl_reg[i].name,"END")==0)break;
   }
        
   if(strcmp(ctrl_reg[i].name,"END")==0){ 
      printf(" Register Name not found :  %s \n",name); 
      return s_defaultErrVal;
   }else{
      //// set address offset
      addr = ctrl_reg[i].address;   
      data_word = read_word( addr );
      if( data_word < 0 ){ 
         return s_defaultErrVal; 
      }

      data_word = (data_word & ctrl_reg[i].mask) >> ctrl_reg[i].start_bit;
   }

    return data_word;
}

//
//! @brief Write the value of a register.
//! @param[in] *ctrl_reg   The control register in struct.
//! @param[in] name        The name of the register.
//! @return Correntness of the VME writing.
//
bool VMEController::write_reg( struct reg::control_reg const *ctrl_reg, 
                               char const name[32], 
                               unsigned long new_value )
{
   unsigned long addr, data_word, value, mask;
   int           status = -1 , i;

   /// find register name
   for(i=0; i<reg::k_MaxNreg; ++i)  // Find Register Name
   {
          if(strcmp(name,ctrl_reg[i].name)==0)break;
          if(strcmp(ctrl_reg[i].name,"END")==0)break;
   }
        
   if(strcmp(ctrl_reg[i].name,"END")==0){ 
      printf(" Register Name not found :  %s \n",name); 
      return false;
   }else{
      addr = ctrl_reg[i].address;   // Set address offset
      mask = ctrl_reg[i].mask;
      value = read_word( addr );    // Get existing word

      data_word = (value & (~mask)) | 
                  ((new_value & (mask>>ctrl_reg[i].start_bit)) << ctrl_reg[i].start_bit);

      if( strcmp(ctrl_reg[i].type,"rw") !=0 ){ 
         printf("    <%s>  %i %s register is Read Only\n",ctrl_reg[i].name,i,name);
         return false;
      }else{
         status = write_word( addr, data_word );   // Write VME Bus
      }
                
      if( status < 0 ) return false; // Check for ERROR
   }        

   return true;
}

//
//! @brief    Read the register value out from the board.
//! @param[in] name The name of the register.
//! @return   The register value.
//! @warning  The board type must be correctly specified in advance.
//
unsigned long VMEController::read_reg( const char name[32] )
{   
   return ( IsBoardRegistered() ) ? read_reg(m_ctrl_reg, name) 
                                  : s_defaultErrVal;
}

//
//! @brief Write the register.
//! @param[in] name       The register name.
//! @param[in] new_value  The value to be written.
//! @return Corretness of the VME writing.
//! @warning The board type must be correctly specified in advance.
//
bool VMEController::write_reg( const char name[32] , unsigned long new_val )
{
   //for( int i=0; i<100; i++ ) write_reg(m_ctrl_reg, name, new_val);
   if( !write_reg(m_ctrl_reg, name, new_val) ) return false;

   int trial_cnt = 0;
   const int max_ntrial = 20;
   while( m_isCheckWriteFlag && new_val != read_reg(name) )
   {
      write_reg(m_ctrl_reg, name, new_val);

      std::cout<<" Register writing error : " << name 
               <<" for board in crate" << m_crateId << "-slot " << m_slotId 
               <<" ( trial # : " << ++trial_cnt << " ) " << std::endl;    
      std::cout<<" Intend to write: " << std::hex << new_val 
               << " but the read-value is: " << (read_reg(name)) 
               << std::dec << std::endl;
      std::cout<<" *** Now re-initialize ... " << std::endl;
      if( trial_cnt==max_ntrial ){
         std::cout<<" Fail to write the register:" << name << std::endl;
         SaveRegisterErr( name , new_val );
         return false;
      }
   }

   return true;
}

//
//! @brief The version code of this board.
//! @return Version code.
//! @warning The board type must be correctly specified in advance.
//
unsigned long VMEController::GetVersion( void )
{
   return (GetBoardName()=="adc") ? read_reg("code_revision") 
                                  : read_reg("version");
}

//
//! @brief Massive VME readout from an given address.
//! @param[in] addr     The address for data readout.
//! @param[in] *buffer  The array to read consecutive words.
//! @param[in] nwords   The amount of the consecutive words to be read.
//! @return The correctness of the VME reading.
//! @warning It ONLY supports ADC boards.
//
bool VMEController::read_blt( unsigned long addr, 
                              unsigned long *buffer, 
                              int nwords )
{

   if( GetBoardName()!="adc" )
     return false;

   vme_bus_handle_t bus_handle;
   vme_dma_handle_t dma_handle;
        
   uint32_t *ptr;
   int nbytes = 4*nwords;

   addr=m_slotId*(0x8000000) + addr;

   if (vme_init(&bus_handle)) {
      perror("Error initializing the VMEbus");
      return false;
   }

   if (vme_dma_buffer_create(bus_handle, &dma_handle, nbytes, 0, NULL)) {
      perror("Error creating the buffer");
      vme_term(bus_handle);
      return false;
   }

   ptr = static_cast<uint32_t*>( vme_dma_buffer_map( bus_handle, 
                                                     dma_handle, 
                                                     0          ) );

   if (!ptr) {
      perror("Error mapping the buffer");
      vme_dma_buffer_release(bus_handle, dma_handle);
      vme_term(bus_handle);
      return false;
   }

   // Transfer the data.
   if (vme_dma_read(bus_handle, dma_handle, 0, addr, ADDRESS_MODIFIER_BLOCK, nbytes, 0) ) {
      perror("Error reading data");
      vme_dma_buffer_unmap(bus_handle, dma_handle);
      vme_dma_buffer_release(bus_handle, dma_handle);
      vme_term(bus_handle);
      return false;
   }

   // Move data from dma buffer to data buffer.
   for(int i=0; i<nwords; ++i ) 
   {
      buffer[i]=*(ptr+i);  
   }

   if (vme_dma_buffer_unmap(bus_handle, dma_handle)) {
      perror("Error unmapping the buffer");
      vme_dma_buffer_release(bus_handle, dma_handle);
      vme_term(bus_handle);
      return false;
   }

   if (vme_dma_buffer_release(bus_handle, dma_handle)) {
      perror("Error releasing the buffer");
      vme_term(bus_handle);
      return false;
   }

   if (vme_term(bus_handle)) {
      perror("Error terminating");
      return false;
   }
        
   return 0;

}

//
// @brief
// 
bool VMEController::SaveRegisterErr( char const* reg_name , unsigned long new_val )
{
   char const* logdir = std::getenv("DAQ_LOG_DIR");
   if( logdir==NULL ){
      std::cout<<"Error in <VMEController::SaveRegisterErr>"
               <<" DAQ_LOG_DIR is not set. " << std::endl;
      return false;
   }

   char ofname[100];
   sprintf( ofname, "%s/run_dump/reg_err/reg_err_run%d.txt", logdir, m_runId );

   std::ofstream ofile( ofname, std::ofstream::app );
   if( ofile.fail() ){
      std::cout<<"Error in <VMEController::SaveRegisterErr>"
               <<" fail to open output file: " << ofname << std::endl;
      return false;
   } 

   ofile << "crate" << GetCrateId() <<" slot" << GetSlotId() <<" register: " << reg_name 
         <<" write in value: " << new_val << " error. " << std::endl;

   return true;
}
