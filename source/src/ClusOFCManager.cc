#include "ClusOFCManager.h"

//
//! @brief Constructor initialized by slot number.
//! @param[in] slotId is the slot number.
//
ClusOFCManager::ClusOFCManager( int slotId ) 
   : VMEController(slotId, "cofc")
{
   Init();
}

//
//! @brief Default destructor.
//
ClusOFCManager::~ClusOFCManager()
{
   ;
}

//
//! @brief Initialization of this board.
//
void ClusOFCManager::Init( void )
{
   SetTestMode(0x1);
   Reset();

   write_reg("ena_lvds_cnt",0x0);
   write_reg("ena_olrx",0x7ff);
}
