#include "TopCDTManager.h"

//
//! @brief Default constructor.
//
TopCDTManager::TopCDTManager( int slotId ) 
   : VMEController( slotId, "tcdt" )
{
   Init();
}

//
//! @brief Default destructor.
//
TopCDTManager::~TopCDTManager()
{
    ;
}

//
//! @brief Initialization of this board.
//
void TopCDTManager::Init( void )
{
   SetTestMode(true);
   Reset();

   // input switch
   // - 0: from fiber.
   // - 1: GND.
   write_reg("fiber_in_sw",0);
   //write_reg("veto_in_switch",0);

   // live mode
   // - 0: from upstream (MACTRIS).
   // - 1: internal LIVE signal.
   write_reg("live_mode",0);   
   write_reg("ena_live_sim",0); 
   write_reg("force_live_on",0);

   write_reg("et_err_in_switch",0);
   write_reg("fcdt_err_out_sw",0);

   write_reg("bypass_tlk_err",0);
   write_reg("bypass_dc_err",0);
   write_reg("bypass_lv2_full",0);
   write_reg("omit_tlk_err_in_cond",0);
   write_reg("ena_olrx",0xf);

   write_reg("send_factory_trig",0);
   write_reg("ena_delta_factory",0);
   write_reg("factory_ntrig",0);
   write_reg("ena_fiber_sim",0);
   write_reg("ena_lvds_cnt_debug",0);
   write_reg("ext_trig_in_switch",0);
   write_reg("ena_delta_lv1",1);
   write_reg("ena_clock_trig",0);
   write_reg("nclus_in_switch",0);

   write_reg("et_thr",0x1);
   write_reg("nspill",502);
   write_reg("delta_et_thr",0x1);
   write_reg("delta_veto_ptn",0x1);

   // set default delay //
   write_reg("delay_raw",e14::k_DelayRaw);
   write_reg("delay_lv1b_req",e14::k_DelayLv1bReq);
   write_reg("delay_lv1",e14::k_DelayLv1);
   write_reg("spill_off_time",e14::k_SpillOffTime);
}

//
//! @brief Standalone mode: generated LIVE from TOP CDT.
//! param[in] isStandalone is to enable standalone mode or not.
//
void TopCDTManager::SetStandaloneMode( bool isStandalone )
{
   write_reg("live_mode", isStandalone);
   write_reg("ena_live_sim", isStandalone );
   write_reg("bypass_lv2_full", isStandalone );
}

//
//! @brief Setting of the internal clock trigger.
//! @param[in] user_interval is the interval in clock (Maximum = 0xfffff).
//!            Internal clock trigger is disabled if user_interval < 1.
//
void TopCDTManager::SetInternalClock( int user_interval )
{
   bool ena_clock = true;
   int  interval  = user_interval;
   if( interval<1 ){
      std::cout<<"Info in <TopCDTManager::SetInternalClock>"
               <<" disable internal clock trigger. " << std::endl;
      ena_clock = false;
   }else if( interval>0xfffff ){
      interval = 0xfffff;
   }

   write_reg("ena_clock_trig", ena_clock);
   write_reg("ext_trig_in_switch", ena_clock);
   write_reg("clock_trig_gap", interval);
}

//
//! @brief Set 8 internal trigger type from the external files.
//! @param[in] type is the name of the run type (Physics, CSIEt, TMON, ...).
//! @return true if the external file is readable.
//
bool TopCDTManager::SetTrigType( char const* type )
{
   for( int itype=0; itype<s_nInternalTrigType; ++itype )
      if( !SetTrigType(type, itype) ) 
         return false;

   return true;
}
   
//
//! @brief Set 4 external trigger type from the external files.
//! @param[in] type is the name of the run type (Physics, CSIEt, TMON, ...).
//! @return true if the external file is readable.
//
bool TopCDTManager::SetExtTrigType( char const* type )
{
   for( int itype=0; itype<s_nExternalTrigType; ++itype )
      if( !SetExtTrigType(type, itype) )
         return false;

   return true;
}

//
//! @brief Set an internal trigger type from the external files.
//! @param[in] type is the name of the run type (Physics, CSIEt, TMON, ...).
//! @return true if the external file is readable.
//
bool TopCDTManager::SetTrigType( char const* runtype, int trigno )
{
   char const* setdir = std::getenv("DAQ_SETTING_DIR");
   if( setdir==NULL ){
      std::cout<<"Error in <TopCDTManager::SetTrigType>"
               <<" DAQ_SETTING_DIR is not set. " << std::endl;
      return false;
   } 

   char fname[100];
   sprintf( fname, "%s/TrigType/%s/TrigType%d.txt", std::getenv("DAQ_SETTING_DIR"), 
                                                    runtype, 
                                                    trigno);
   std::ifstream ifile( fname );

   if( ifile.fail() ){
      std::cout<<"Error in <TopCDTManager::SetTrigType>"
               <<" Fail to read trigger type setting, unknown runtype : " 
               << runtype << std::endl;
      return false; 
   }

   char rname[32], reg_name[32]; 
   int rval = 0;
   while( ifile >> rname >> std::hex >> rval )
   {
      sprintf( reg_name, "type%d_%s", trigno, rname );
      write_reg( reg_name , rval );         
   }

   return true;
}

//
//! @brief Set an external trigger type from the external files.
//! @param[in] type is the name of the run type (Physics, CSIEt, TMON, ...).
//! @return true if the external file is readable.
//
bool TopCDTManager::SetExtTrigType(char const* runtype, int trigno)
{
   char const* setdir = std::getenv("DAQ_SETTING_DIR");
   if( setdir==NULL ){
      std::cout<<"Error in <TopCDTManager::SetExtTrigType>"
               <<" DAQ_SETTING_DIR is not set. " << std::endl;
      return false;
   }

   char fname[100];
   sprintf( fname, "%s/TrigType/%s/Ext%d.txt", setdir, runtype, trigno);
   std::ifstream ifile( fname );

   if( ifile.fail() ){ 
      std::cout<<"Error in <TopCDTManager::SetExtTrigType> "
               <<" Fail to read external trigger type setting, unknown runtype: " 
               << runtype << std::endl;
      return false;
   }

   char rname[32], reg_name[32];
   int rval = 0;
   while( ifile >> rname >> std::hex >> rval )
   {
      sprintf( reg_name, "ext%d_%s", trigno, rname );
      write_reg( reg_name , rval );
   }

   return true;
}

//
//! @brief Update of the error dignosis result of this spill.
//
void TopCDTManager::UpdateErr()
{
   m_isTLKErr = read_reg("tlk_err");
   m_isDCErr = read_reg("dc_err");
   m_isLocalAlignErr = read_reg("local_align_err");
   m_isGlobalAlignErr = read_reg("global_align_err");
   m_isClusDeltaErr = read_reg("clus_delta_err");
   m_isClusIdErr = read_reg("clus_id_err");
   m_isClusTimeErr = read_reg("clus_id_err");

   m_EtAlignPtnTime = read_reg("delay_et_align_ptn");
   m_Veto0AlignPtnTime = read_reg("delay_veto0_align_ptn");
   m_Veto1AlignPtnTime = read_reg("delay_veto1_align_ptn");
   m_EtDeltaPtnTime = read_reg("delay_et_delta_ptn");
   m_Veto0DeltaPtnTime = read_reg("delay_veto0_delta_ptn");
   m_Veto1DeltaPtnTime = read_reg("delay_veto1_delta_ptn");
   m_DelayClusTime = read_reg("delay_clus_result");
   m_EarlyLv1Time = read_reg("lv1_delay");

   m_EtDeltaPtn = read_reg("delta_et_raw");
   m_VetoDeltaPtn = read_reg("delta_veto_raw");
   m_EtDeltaLv1aTime = read_reg("et_delta_lv1a_t");
   m_VetoDeltaLv1aTime = read_reg("veto_delta_lv1a_t");
}

//
//! @brief Update of the DAQ statistics of this spill.
//
void TopCDTManager::UpdateCounter()
{
   m_nDelta = read_reg("ndelta");
   m_lv1aRawCnt = read_reg("lv1a_raw_cnt");
   m_lv1aCnt = read_reg("lv1a_cnt");
   m_lv1aRejCnt = read_reg("lv1a_rej_cnt");
   m_lv1Cnt = read_reg("lv1_cnt");
   m_lv1PsCnt = read_reg("lv1_ps_cnt");
   m_lv2RejCnt = read_reg("lv2_rej_cnt");

   char reg_name[32];

   for( int i=0; i<s_nClusterCounter; i++ )
   {
      if( i < s_nClusterCounter-1 ){
         sprintf(reg_name,"scaled_clus%d_cnt",i);
         m_scaledClusCnt[i] = read_reg(reg_name);
      }else{
         m_scaledClusCnt[i] = read_reg("scaled_clusN_cnt");
      }
   }  

   for( int i=0; i<s_nInternalTrigType; i++ )
   {
      sprintf(reg_name,"type%d_lv1a_scaled_cnt",i);
      m_typeLv1aScaledCnt[i] = read_reg(reg_name);
   }

   for( int i=0; i<s_nInternalTrigType; i++ )
   {
      sprintf(reg_name,"type%d_lv1b_scaled_cnt",i);
      m_typeLv1bScaledCnt[i] = read_reg(reg_name);
   }

   for( int i=0; i<s_nVetoCounter; i++ )
   {
      sprintf(reg_name,"veto%d_cnt",i);
      m_vetoCnt[i] = read_reg(reg_name);
   }

   for( int i=0; i<s_nExternalTrigType; i++ )
   {
      sprintf(reg_name,"ext%d_cnt",i);
      m_extCnt[i] = read_reg(reg_name);
   }

   for( int i=0; i<s_nExternalDataCounter; i++ )
   {
      sprintf(reg_name,"exdata%d",i);
      m_extData[i] = read_reg(reg_name);
   }

   m_etCnt = read_reg("et_cnt");
}

//
//! @brief Print out the error message on the display.
//! @warning The contents need to be updated by TopCDTManager::UpdateErr(int).
//
void TopCDTManager::PrintErr( void )
{
   std::cout<<" Error monitoring... " << std::endl;
   std::cout<<" [0] TLK error                : " << m_isTLKErr << std::endl;
   std::cout<<" [1] DC / veto error          : " << m_isDCErr << std::endl;
   std::cout<<" [2] Local align err          : " << m_isLocalAlignErr << std::endl;
   std::cout<<" [3] Global align err         : " << m_isGlobalAlignErr << std::endl;
   std::cout<<" [4] Cluster delta trig err   : " << m_isClusDeltaErr << std::endl;
   std::cout<<" [5] Cluster ID err           : " << m_isClusIdErr  << std::endl;
   std::cout<<" [6] Cluster timing err       : " << m_isClusTimeErr << std::endl;

   std::cout<< std::dec << std::endl;
   std::cout<<" Timing test result: " << std::endl;
   std::cout<<" [0] ET align pattern time    : " << m_EtAlignPtnTime << std::endl;
   std::cout<<" [1] Veto0 align time         : " << m_Veto0AlignPtnTime << std::endl;
   std::cout<<" [2] Veto1 align time         : " << m_Veto1AlignPtnTime << std::endl;
   std::cout<<" [3] ET delta pattern time    : " << m_EtDeltaPtnTime << std::endl;
   std::cout<<" [4] Veto0 delta pattern time : " << m_Veto0DeltaPtnTime << std::endl;
   std::cout<<" [5] Veto1 delta pattern time : " << m_Veto1DeltaPtnTime << std::endl;
   std::cout<<" [6] T to get clus from plv1  : " << m_DelayClusTime << std::endl;
   std::cout<<" [7] Early lv1 decision time  : " << m_EarlyLv1Time << std::endl;

   std::cout<< std::hex << std::endl;
   std::cout<<" Delta pattern monitoring: " << std::endl;
   std::cout<<" [0] Delta ET raw (in hex)    : " << m_EtDeltaPtn << std::endl;
   std::cout<<" [1] Delta veto raw (in hex)  : " << m_VetoDeltaPtn << std::endl;
   std::cout<< std::dec;
   std::cout<<" [2] Delta ET time (lv1a)     : " << m_EtDeltaLv1aTime << std::endl;
   std::cout<<" [3] Delta veto time (lv1a)   : " << m_VetoDeltaLv1aTime << std::endl;
}

//
//! @brief Print out the DAQ statistics.
//
void TopCDTManager::PrintCounter( void )
{
   std::cout<< std::dec << std::endl;
   std::cout<<" N delta ptn       : " << m_nDelta << std::endl;
   std::cout<<" Lv1a raw cnt      : " << m_lv1aRawCnt << std::endl;
   std::cout<<" Lv1a accepted cnt : " << m_lv1aCnt << std::endl;
   std::cout<<" Lv1a rejected cnt : " << m_lv1aRejCnt << std::endl;
   std::cout<<" Lv1 cnt           : " << m_lv1Cnt << std::endl;
   std::cout<<" Lv1 w/ ps         : " << m_lv1PsCnt << std::endl;
   std::cout<<" Lv2 full rej cnt  : " << m_lv2RejCnt << std::endl;
   std::cout<< std::endl;

   std::cout<<" Cluster counter (internal, lv1 accepted) " << std::endl;
   for( int i=0; i<s_nClusterCounter; i=i+2 )
   {
      std::cout<<"  - " << i   <<" cluster      : " << m_scaledClusCnt[i]  << "\t\t "
               <<"  - " << i+1 <<" cluster      : " << m_scaledClusCnt[i+1]  << std::endl;
   }
   std::cout<<std::endl;

   std::cout<<" Lv1a Trigger type counter" << std::endl;
   for( int i=0; i<s_nInternalTrigType; i=i+2 )
   {
      std::cout<<"  - type " << i   <<" scaled : " << m_typeLv1aScaledCnt[i]  << "\t\t "
               <<"  - type " << i+1 <<" scaled : " << m_typeLv1aScaledCnt[i+1]  << std::endl;
   }
   std::cout<< std::endl;

   std::cout<<" Lv1b Trigger type counter" << std::endl;
   for( int i=0; i<s_nInternalTrigType; i=i+2 )
   {
      std::cout<<"  - type " << i   <<" scaled : " << m_typeLv1bScaledCnt[i]  << "\t\t "
               <<"  - type " << i+1 <<" scaled : " << m_typeLv1bScaledCnt[i+1]  << std::endl;
   }

   std::cout<<" Veto counter (rising edge, independent of trigger decision) " << std::endl;
   for( int i=0; i<s_nVetoCounter; i =i+2 )
   {
      std::cout<<"  - veto " << i   <<"\t        : " << m_vetoCnt[i]  << "\t\t "
               <<"  - veto " << i+1 <<"\t        : " << m_vetoCnt[i+1]  << std::endl;
   }
   std::cout<< std::endl;

   std::cout<<" External trigger counter " << std::endl;
   std::cout<<"  - ext 0          : " << m_extCnt[0]  << "\t\t "
            <<"  - ext 1          : " << m_extCnt[1]  << std::endl;
   std::cout<< std::endl;

   std::cout<<" External data (SEC,TMON) " << std::endl;
   std::cout<<"  - ext 0          : " << m_extData[0]  << "\t\t "
            <<"  - ext 1          : " << m_extData[1]  << std::endl;
   std::cout<<std::endl;

   std::cout<<" CSI Et count : " << m_etCnt << std::endl;
}

//
//! @brief Save error monitoring result in a text file.
//! @param[in] runId and spillId are the run ID and spill number for the name of error output file.
//! @warning The UpdateErr() should be called in advance to update the contents.
//
bool TopCDTManager::SaveErr( int runId, int spillId )
{
   char const* errdir = std::getenv("ERR_OUT_DIR");
   if( errdir==NULL ){
      std::cout<<"Error in <TopCDTManager::SaveErr>"
               <<" ERR_OUT_DIR is not set. " << std::endl;
      return false;
   }

   char ofname[100];
   sprintf( ofname, "%s/run%d/Errlog_run%d_spill%d.txt", errdir, runId, runId, spillId );

   std::ofstream ofile( ofname );

   if( ofile.fail() ){
      std::cout<<"Error in <TopCDTManager::SaveErr>"
               <<" fail to open output file: " << ofname << std::endl;
      return false;
   }

   ofile <<"tlk_err\t" << m_isTLKErr << std::endl;
   ofile <<"dc_err\t" << m_isDCErr << std::endl;
   ofile <<"local_align_err\t" << m_isLocalAlignErr << std::endl;
   ofile <<"global_align_err\t" << m_isGlobalAlignErr << std::endl;
   ofile <<"clus_delta_err\t" << m_isClusDeltaErr << std::endl;
   ofile <<"clus_id_err\t" << m_isClusIdErr  << std::endl;
   ofile <<"clus_time_err\t" << m_isClusTimeErr << std::endl;

   ofile <<"et_align_ptn_time\t" << m_EtAlignPtnTime << std::endl;
   ofile <<"veto0_align_ptn_time\t" << m_Veto0AlignPtnTime << std::endl;
   ofile <<"veto1_align_ptn_time\t" << m_Veto1AlignPtnTime << std::endl;
   ofile <<"et_delta_ptn_time\t" << m_EtDeltaPtnTime << std::endl;
   ofile <<"veto0_delta_pattern_time\t" << m_Veto0DeltaPtnTime << std::endl;
   ofile <<"veto1_delta_pattern_time\t" << m_Veto1DeltaPtnTime << std::endl;
   ofile <<"time_to_get_clus_from_plv1\t" << m_DelayClusTime << std::endl;
   ofile <<"early_lv1_decision_time\t" << m_EarlyLv1Time << std::endl;

   ofile << std::hex;
   ofile <<"delta_et_raw\t" << m_EtDeltaPtn << std::endl;
   ofile <<"delta_veto_raw\t" << m_VetoDeltaPtn << std::endl;
   ofile << std::dec;
   ofile <<"delta_et_lv1a_time\t" << m_EtDeltaLv1aTime << std::endl;
   ofile <<"delta_veto_lv1a_time\t" << m_VetoDeltaLv1aTime << std::endl;

   ofile.close();
   return true;
}

//
//! @brief Save counter result in a text file.
//! @param[in] runId and spillId are the run ID and spill number for the name of counter output file.
//! @warning The UpdateCounter() should be called in advance to update the contents.
//
bool TopCDTManager::SaveCounter( int runId, int spillId )
{
   char const* cntdir = std::getenv("CNT_OUT_DIR");
   if( cntdir==NULL ){
      std::cout<<"Error in <TopCDTManager::SaveCounter>"
               <<" CNT_OUT_DIR is not set. " << std::endl;
      return false;
   }

   char ofname[100];
   sprintf( ofname, "%s/run%d/run%d_spill%d.txt", cntdir, runId, runId, spillId );

   std::ofstream ofile( ofname );
   if( ofile.fail() ){
      std::cout<<"Error in <TopCDTManager::SaveErr>"
               <<" fail to open output file: " << ofname << std::endl;
      return false;
   }

   ofile <<"ndelta\t" << m_nDelta << std::endl;
   ofile <<"lv1a_raw_cnt\t"<< m_lv1aRawCnt << std::endl;
   ofile <<"lv1a_cnt\t" << m_lv1aCnt << std::endl;
   ofile <<"lv1a_rej_cnt\t" << m_lv1aRejCnt << std::endl;
   ofile <<"lv1_cnt\t" << m_lv1Cnt << std::endl;
   ofile <<"lv1_ps_cnt\t"<< m_lv1PsCnt << std::endl;
   ofile <<"lv2_rej_cnt\t"<< m_lv2RejCnt << std::endl;

   for( int i=0; i<s_nClusterCounter; i++ )
      ofile << "scaled_cluster" << i << "\t" << m_scaledClusCnt[i] << std::endl;

   for( int i=0; i<s_nInternalTrigType; i++ )
   {
      ofile << "type_lv1a_scaled" << i <<"\t" << m_typeLv1aScaledCnt[i] << std::endl;
      ofile << "type_lv1b_scaled" << i <<"\t" << m_typeLv1bScaledCnt[i] << std::endl;
   }

   for( int i=0; i<s_nVetoCounter; i++ )
      ofile << "veto" << i   <<"\t" << m_vetoCnt[i]  << std::endl;
   
   ofile << "csi\t" << m_etCnt << std::endl;
   ofile << "ext0\t"<< m_extCnt[0]  << std::endl;
   ofile << "ext1\t"<< m_extCnt[1]  << std::endl;
   ofile << "SEC\t" << m_extData[0]  << std::endl;
   ofile << "TMON\t" << m_extData[1]  << std::endl;
}

