#include "LocalCDTManager.h"

//
// @brief The constructor initialized by slot number.
//
LocalCDTManager::LocalCDTManager( int slotId ) 
   : VMEController( slotId, "lcdt" )
{
   Init();
}

//
//! @brief Default destructor.
//
LocalCDTManager::~LocalCDTManager()
{
   ;
}

//
//! @brief Initialization of this board.
//
void LocalCDTManager::Init( void )
{
   SetTestMode(true);
   Reset();

   write_reg("err_adc_masking", 0x0);
   write_reg("ena_digout_debug", 0x0);
   write_reg("send_factory_trig", 0x0);
   write_reg("lvds_debug_mode", 0x0);
   write_reg("write_err_sim_data", 0x0);
   write_reg("lvds_in_switch", 0x0);
   write_reg("ena_err_sim_trig", 0x0);
   write_reg("tlk_olrx_masking", 0x0);
   write_reg("dc_olrx_masking", 0x0);
   write_reg("ena_lvds_counter", 0x0);
   
   write_reg("tlk_err_max_time", e14::k_LocalCDTTLKErrMaxTime);
   write_reg("dc_err_max_time", e14::k_LocalCDTDCErrMaxTime);

   SetDefaultBitMasking();
   SetDefaultHitCountRange();
}

//
//! @brief Selection of the DAQ mode to analyze the input data.
//
void LocalCDTManager::SetAnalysisMethod( e14::E_AnalysisMethod method )
{
   switch( method )
   {
      case e14::k_VetoSingleThr:
         write_reg("is_veto_crate",0x1);
         write_reg("is_single_thr",0x1);
         break;

      case e14::k_VetoDualThrWithHitCount:
         write_reg("is_veto_crate",0x1);
         write_reg("is_single_thr",0x0);
         write_reg("ena_cvhit0",0x1);
         write_reg("ena_cvhit1",0x1);
         SetDaisyChain();
         break;

      case e14::k_VetoDualThrWoHitCount:
         write_reg("is_veto_crate",0x1);
         write_reg("is_single_thr",0x0);
         write_reg("ena_cvhit0",0x0);
         write_reg("ena_cvhit1",0x0);
         SetDaisyChain();
         break;

      case e14::k_ET:
         write_reg("is_veto_crate",0x0);
         write_reg("is_single_thr",0x0);
         SetDaisyChain();
         break;

      default:
         std::cout<<"Error in <LocalCDTManager::SetDAQMode> unknown method: " 
                  << method << std::endl;
   }
}

//
//! @brief Settings to accept the Daisy Chain results and perform alignment.
//
void LocalCDTManager::SetDaisyChain( void )
{
   write_reg("ena_olrx", 0xf);
   write_reg("et_maxt", e14::k_LocalCDTAlignMaxTime);

   write_reg("et_masking", 0x0);
   write_reg("et_ptn_rx0", 0xffff);
   write_reg("et_ptn_rx1", 0xffff);
   write_reg("et_ptn_rx2", 0xffff);
   write_reg("et_ptn_rx3", 0xffff);

   write_reg("tlk_olrx_masking",0x0);
   write_reg("dc_olrx_masking",0x0);
}

//
//! @brief Settings of bit shift for single threshold strategy.
//! @param[in] svec is the vector contains 16 bit shifts in the sequence of FADC ID. 
//!            The valid range is 0-15 and the size of this vector should be exact e14::k_nFADC.
//! @return The corretness of the input variables.
//
bool LocalCDTManager::SetSingleVetoBitShift( std::vector<int> const& svec )
{
   if( svec.size()!=e14::k_nFADC ){
      std::cout<< "Error in <LocalCDTManager::SetSingleVetoBitShift>"
               <<" invalid svec size: " << svec.size() <<std::endl; 
      return false;
   }

   int fadcId = 0;
   for(std::vector<int>::const_iterator it = svec.begin(); it!=svec.end(); ++it )
   {
      if( *it<0 || *it>=e14::k_nFADC ){
         std::cout<<"Warning in <LocalCDTManager::SetSingleVetoBitShift>"
                  <<" FADC-" << fadcId << ": intend to write invalid bit: " << *it << std::endl;
         return false;
      }

      char reg_name[32];
      sprintf( reg_name, "ch%d_detbit", fadcId++ );
      write_reg(reg_name, *it);
   }

   return true;
}

//
//! @brief Settings of bit masking for single threshold strategy.
//! @param[in] mvec is the vector contains 16 bit masking in the sequence of FADC ID.
//!            The size of this vector should be exact e14::k_nFADC.
//! @return The corretness of the input variables.
//
bool LocalCDTManager::SetSingleVetoBitMasking( std::vector<bool> const& mvec )
{
   if( mvec.size()!=e14::k_nFADC ){
      std::cout<< "Error in <LocalCDTManager::SetSingleVetoBitShift>"
               <<" invalid mvec size: " << mvec.size() <<std::endl;
      return false;
   }

   int fadcId = 0;
   unsigned long val = 0;
   for(std::vector<bool>::const_iterator it = mvec.begin(); it!=mvec.end(); ++it )
      val |= ( *it << (fadcId++) );

   SetDiginMasking(val);
   return true;
}

//
//! @brief Setting of dual veto bit shift. (for Daisy-Chain method.)
//! @param[in] svec is the amount of the shift for each bit. The valid range is from 0 to 15.
//             The size of this vector should be exact e14::k_nDaisyChainMember x 2.
//! @param[in] inputId is the fiber input number of LOCAL CDT (range: 0-3).
//
bool LocalCDTManager::SetDualVetoBitShift( int inputId, std::vector<int> const& svec )
{
   if( inputId<0 || inputId>=e14::k_nDaisyChain ){
      std::cout<<"Error in <LocalCDTManager::SetDualVetoBitShift>"
               <<" invalid inputId: " << inputId << std::endl;
      return false;
   }

   if( svec.size()!=e14::k_nDaisyChainMember*2 ){
      std::cout<<"Error in <LocalCDTManager::SetDualVetoBitShift>"
               <<" invalid size of svec: " << svec.size() << std::endl;
      return false; 
   }

   unsigned long val = 0;
   int ibit = 0;
   for(std::vector<int>::const_iterator it=svec.begin(); it!=svec.end(); ++it )
   {
      if( *it<0 || *it>=e14::k_nBitOfOLWord ){
         std::cout<<"Error in <LocalCDTManager::SetDualVetoBitShift>"
                  <<" invalid shift: " << *it << std::endl;
         return false;
      }

      // Rule for bit definition register:
      // - All of the dual veto results are recorded in [7:0] of an OL input.
      // - Input [1:0] is the pair for rank 0, [3:2] is the pair for rank 1, ...
      // - The first bit of a pair is the result from threshold 0, 2nd bit from threshold 1.
      // - Register "bit_shift_x0" is the bit definition for OL input x at range of [7:0]
      // [3 : 0] bit definition for input bit [0]
      // [7 : 4] bit definition for input bit [1]
      // ...
      // [31:28] bit definition for input bit [7]

      val |= ( (*it) << (ibit++)*e14::k_nBitOfShift );
   }

   char reg_name[32];
   sprintf( reg_name, "bit_shift_%d0", inputId );
   write_reg( reg_name, val );
   return true;
}

//
//! @brief Setting of bit masking for dual veto method. (for Daisy Chain method.)
//! @param[in] mvec is the masking of each bit. 
//!            The size should be exact e14::k_nDaisyChainMember x 2.
//! @param[in] inputId is the fiber input number of LOCAL CDT (range: 0-3).
//
bool LocalCDTManager::SetDualVetoBitMasking( int inputId, std::vector<bool> const& mvec )
{
   if( inputId<0 || inputId>=e14::k_nDaisyChain ){
      std::cout<<"Error in <LocalCDTManager::SetDualVetoBitMask>"
               <<" invalid inputId: " << inputId << std::endl;
      return false;
   }

   if( mvec.size()!=e14::k_nDaisyChainMember*2 ){
      std::cout<<"Error in <LocalCDTManager::SetDualVetoBitMask>"
               <<" invalid size of mvec: " << mvec.size() << std::endl;
      return false;
   }

   unsigned long val = 0;
   int ibit = 0;
   for(std::vector<bool>::const_iterator it=mvec.begin(); it!=mvec.end(); ++it )
      val |= ( *it << (ibit++) );
   
   char reg_name[32];
   sprintf( reg_name, "bit_mask_in%d", inputId );
   unsigned long r_val = read_reg( reg_name );
   write_reg( reg_name, (r_val&0xff00) + val );
   return true;
}

//
//! @brief Settings of single veto bits from an external file.
//!
//! The file name should be Crate%d_BitMap.txt under DAQ_SETTING_DIR/BitMap.
//! Format:
//!     "input no"      "shift"       "mask"
//!     0               ?               ?
//!     1               ?               ?               
//!     ..
//!     15              ?               ?               
//!
//! The contents are required in the sequence of input (via LVDS).
//! The expected number of rows is 16(inputs), representing 16 ADCs.
//! The shift is in the range of 0-15.
//!
//! @param[in] crateId is the crate ID for the local crate.
//
bool LocalCDTManager::SetSingleVetoBitFromExternalFile(int crateId, char const* type )
{
   char ifname[100];
   sprintf(ifname, "BitMap/%s/Crate%d_BitMap.txt", type, crateId );
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   int r_in, r_shift;
   bool r_mask;
   std::vector<int> svec;
   std::vector<bool> mvec;
   while( ifile >> r_in >> r_shift >> r_mask )
   {
      svec.push_back(r_shift);
      mvec.push_back(r_mask);

      if( r_in+1 != svec.size() ){
         std::cout<<"Error in <LocalCDTManager::SetSingleVetoBitFromExternalFile>"
                  <<" input ID is not in sequence. " << std::endl;
         return false;
      }
   }

   SetSingleVetoBitShift(svec);
   SetSingleVetoBitMasking(mvec);
   return true; 
}

//
//! @brief Setting of the bit shift for the result of hit count.
//! @param[in] result Id is the hit count result ID. The valid range is 0-1.
//! @param[in] type1_shift is the shift for type 0 hit count result.
//! @param[in] type2_shift is the shift for type 1 hit count result.
//! @return The validity of the input parameter.
//
bool LocalCDTManager::SetHitCountBitShift( int resultId, int type1_shift, int type2_shift )
{
   if( resultId<0 || resultId>=2 ){
      std::cout<<"Error in <LocalCDTManager::SetHitCountBitShift>"
               <<" invalid input result ID: " << resultId << std::endl;
      return false;
   }

   if(    type1_shift<0 || type1_shift>=e14::k_nBitOfOLWord
       || type2_shift<0 || type2_shift>=e14::k_nBitOfOLWord )
   {
      std::cout<<"Error in <LocalCDTManager::SetHitCountBitShift>"
               <<" invalid bit shift for type 1 or type 2: "
               << type1_shift << " , " << type2_shift << std::endl;
      return false;
   }

   // Rule for bit definition register:
   // - hit results are replaced in [15:14] of input 0 (1) for threshold set 0 (1).
   // - [14] type 1
   // - [15] type 2
   const int k_type1Bit = 14;
   const int k_type2Bit = 15;
   unsigned long val = ( type1_shift << (k_type1Bit-8)*4 ) | ( type2_shift << (k_type2Bit-8)*4 );
   char reg_name[32];
   sprintf( reg_name, "bit_shift_%d1", resultId );
   write_reg( reg_name, val );

   sprintf( reg_name, "bit_mask_in%d", resultId );
   unsigned long r_maskval = read_reg( reg_name);
   write_reg( reg_name, (r_maskval&0x3fff) );

   return true;
}

//
//! @brief Setting of hit count range. The signal is determined by the hit count in between.
//! @param[in] resultId is the result ID for different count range. (valid range: 0-1)
//! @param[in] min_count is the lower threshold (and equal to) of the hit count for this result.
//! @param[in] max_count is the higher threshold (and equal to) of the hit count for this result.
//! @return The correctness of the input variables.
//
bool LocalCDTManager::SetHitCountRange( int resultId, int min_count, int max_count )
{
   const int k_CountLimit = 64;
   if( resultId<0 || resultId>=2 ){
      std::cout<<"Error in <LocalCDTManager::SetHitCountRange>"
              <<" invalid result ID: " << resultId << std::endl;
      return false; 
   }else if( max_count >= k_CountLimit ){
      std::cout<<"Error in <LocalCDTManager::SetHitCountRange>"
              <<" max_count only has 6 bits but was set to be " << max_count << std::endl;
      return false;
   }

   char reg_name[32];
   sprintf( reg_name, "fcvhit%d_lthr", resultId );
   write_reg( reg_name, min_count );
   sprintf( reg_name, "fcvhit%d_hthr", resultId );
   write_reg( reg_name, max_count );
   sprintf( reg_name, "rcvhit%d_lthr", resultId );
   write_reg( reg_name, min_count );
   sprintf( reg_name, "rcvhit%d_hthr", resultId );
   write_reg( reg_name, max_count );

   return true;
}

bool LocalCDTManager::SetHitCountRangeFromExternalFile( int crateId )
{
   char ifname[100];
   sprintf( ifname, "HitCount/Crate%d_HitCountRange.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   int r_in, r_min, r_max;
   while( ifile >> r_in >> r_min >> r_max )
   {
      SetHitCountRange( r_in, r_min, r_max );
   }

   return true;
}

//
//! @brief Settings of dual veto bits from an external file.
//!   
//! The file name should be Crate%d_BitMap.txt under DAQ_SETTING_DIR/BitMap.
//! Format:
//! 	"input no"	"bit no."	"shift"		"mask"
//!	0		0		?		?
//!	0		1		?		?
//!	..
//!	0		7		?		?
//!	1		0		?		?
//!	1		1		?		?
//!	..
//!	3		7		?		? 
//! 
//! The contents are required in the sequence of input and bit number.
//! The expected number of rows is 8(bits) x 4(inputs) = 32
//!
//! @param[in] crateId is the crate ID for the local crate.
//
bool LocalCDTManager::SetDualVetoBitFromExternalFile( int crateId, char const* type )
{
   char ifname[100];
   sprintf(ifname, "BitMap/%s/Crate%d_BitMap.txt", type, crateId );
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;
   
   int r_in, r_bit, r_shift;
   bool r_mask;
   std::vector<int> svec;
   std::vector<bool> mvec;
   while( ifile >> r_in >> r_bit >> r_shift >> r_mask )
   {
      if( r_bit != svec.size() ){
         std::cout<<"Error in <LocalCDTManager::SetDualVetoBitFromExternalFile>"
                  <<" the bit number is not in sequence." << std::endl;
         return false;
      }

      svec.push_back(r_shift);
      mvec.push_back(r_mask);

      if( svec.size()==e14::k_nDaisyChainMember*2 )
      {
         if( !SetDualVetoBitShift( r_in, svec ) ) return false;
         if( !SetDualVetoBitMasking( r_in, mvec ) ) return false;

         svec.clear();
         mvec.clear();
      }
   }

   return true; 
}

//
//! @brief Setting of the masking of LVDS input.
//! @param[in] mask_val is a 16-bit word representing the 16 LVDS inputs at LOCAL CDT. 
//
void LocalCDTManager::SetDiginMasking( unsigned long mask_val )
{
   write_reg("err_adc_masking", mask_val);
   write_reg("et_masking", mask_val);
   write_reg("veto_masking", mask_val);
}

//
//! @brief Setting of masking from LVDS input at a LOCAL CDT from an external file.
//!
//! The file name should be Crate%d_DaisyChain.txt under DAQ_SETTING_DIR/DaisyChain.
//! Format:
//!     "input no"      "#lack" 
//!     0               ?          
//!     1               ? 
//!     2               ? 
//!     3               ? 
//!
//! The expected number of rows is 4 (#Daisy Chain inputs). 
//! #lack is at range of 0-4.
//!
//! @param[in] crateId is the crate ID counted from 0.
//
bool LocalCDTManager::SetDiginMaskingFromDaisyChainFile( int crateId )
{
   char ifname[100];
   sprintf(ifname, "DaisyChain/Crate%d_DaisyChain.txt", crateId );
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   int r_in, r_lack;
   unsigned long val = 0;
   while( ifile >> r_in >> r_lack )
   {
      if( r_in<0 || r_in>=e14::k_nDaisyChain || r_lack<0 || r_lack>e14::k_nDaisyChainMember ){
         std::cout<<"Error in <LocalCDTManager::SetDiginMaskingFromExternalFile>"
                  <<" invalid input ID or #lack. " << std::endl;
         return false;
      }

      for( int irank=0; irank<r_lack; ++irank )
      {
         int fadcId = E14DAQFunction::GetFADCIdFromDaisyChainRank(r_in, irank);
         val |= ( 1 << fadcId );
      }
   }

   SetDiginMasking(val);
   return true;
}

//
//! @brief Settings of the hit count bit from an external file.
//!
//! The file name should be Crate%d_HitBitMap.txt under DAQ_SETTING_DIR/BitMap.
//! Format:
//!     "input no"      "bit no."	"shift"		"mask"
//!     0               14		?		?
//!     0               15		?		?
//!     1               14		?		?
//!     1               15		?		?
//!
//! The expected number of rows is 4 at most. The input no. should be 0 or 1 and the bit no. 
//! should be 14 or 15. The range of the shift is 0-15. The meaning of input no. and bit no. 
//! can refer to <LocalCDTManager::SetHitCountBit>.
//!
//! "mask" is forced to be off for hit count all the time.
//!
//! @param[in] crateId is the crate ID counted from 0.
//
bool LocalCDTManager::SetHitCountBitFromExternalFile( int crateId, char const* type )
{
   char ifname[100];
   sprintf(ifname, "BitMap/%s/Crate%d_HitCountBitMap.txt", type, crateId );
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   int r_in, r_bit, r_shift;
   bool r_mask;
   std::vector<unsigned long> svalvec(2,0);
   std::vector<unsigned long> bvalvec(2,0);

   while( ifile >> r_in >> r_bit >> r_shift >> r_mask )
   {
      // input 0 [14] type 1 hit count of threshold set 0
      // input 0 [15] type 2 hit count of threshold set 0
      // input 1 [14] type 1 hit count of threshold set 1
      // input 1 [15] type 2 hit count of threshold set 1
      
      if( r_in<0 || r_in>=2 || r_bit<14 || r_bit>15 ){
         std::cout<<"Error in <LocalCDTManager::SetHitCountBitFromExternalFile>"
                  <<" invalid input no. : " << r_in 
                  <<" or bit no. : " << r_bit << std::endl;
         return false; 
      }

      // register "bit_shift_x1" is the bit shift for [15:8]. 
      // [3 :0 ] for input [8]
      // [7 :4 ] for input [9]
      // ...
      // [31:28] for input [15]
 
      svalvec[r_in] |= ( r_shift << (r_bit-8)*4 );
      bvalvec[r_in] |= ( 1 << r_bit );
   }

   write_reg("bit_shift_01", svalvec[0]);
   write_reg("bit_shift_11", svalvec[1]);
  
   unsigned long mval0 = ( read_reg("bit_mask_in0") & 0x3fff ) | (bvalvec[0] ^ 0xc000 );
   unsigned long mval1 = ( read_reg("bit_mask_in1") & 0x3fff ) | (bvalvec[1] ^ 0xc000 );
   write_reg("bit_mask_in0", mval0); 
   write_reg("bit_mask_in1", mval1); 

   return true;  
}

//
//! @brief Masking of [15:8] for all 4 optical inputs as the default.
//
void LocalCDTManager::SetDefaultBitMasking( void )
{
   write_reg("bit_mask_in0", 0xff00 );
   write_reg("bit_mask_in1", 0xff00 );
   write_reg("bit_mask_in2", 0xff00 );
   write_reg("bit_mask_in3", 0xff00 );
}

//
//! @brief Setting of the default count range of
//!
//! Hit count range 0 : 1 <= result0 <= 2
//! Hit count range 1 : 3 <= result1 <= 4
//
void LocalCDTManager::SetDefaultHitCountRange( void )
{
   //                 resultId, min_count, max_ount
   SetHitCountRange( 0      , 1        , 2         );
   SetHitCountRange( 1      , 3        , 4         );
}
