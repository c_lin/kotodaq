#include "FADCManager.h"

//
//! @brief Basic constructor intialized by slot number only.
//! @param[in] slodId The slot number of this crate.
//
FADCManager::FADCManager( int slotId, int id )
   : VMEController(slotId, "adc"),
     m_id(id)
{
   SetBoardType();
}

//
//! @brief Constructor for the standard KOTO data collection.
//! @param[in] crateId The crate ID counted from 0 through 17.
//! @param[in] slotId The slot number of this board.
//! @param[in] runId The run ID for the DB readout via VME backplane.
//
//FADCManager::FADCManager( int crateId , int slotId , int runId ) 
//   : VMEController(slotId, "adc")
//{
//   SetCrateId(crateId);
//   m_runId = runId;
//   SetBoardType();
//}

//
//! @brief Default destructor.
//
FADCManager::~FADCManager()
{
   ;
}

//
//! @brief Set the board type from the version code.
//
void FADCManager::SetBoardType( void )
{
   if( GetVersion() >= 2200 && GetVersion() < 5000 ){
      m_boardType = k_125MHzADC;
   }else if( GetVersion() >= 5000 ){
      m_boardType = k_500MHzADC;
   }else if( GetVersion() < 2200 && GetVersion() >= 2000 ){
      m_boardType = k_TrigTagADC;
   }else{
      std::cout<<"Warning in <FADCManager::SetBoardType> unknown ADC board at"
               <<" crate" << GetCrateId() 
               <<" slot" << GetSlotId()
               <<"." << std::endl;
      m_boardType = k_UnknownADC;
   }
}

//
//! @brief Set all essential registers as the default values.
//
void FADCManager::Init( void )
{
   if( GetBoardType()==k_UnknownADC ){
      std::cout<<"Error in <FADCManager::Init> unknown ADC board." << std::endl;
      return;
   }

   write_reg("use_zero",0);
   write_reg("ena_daq_mode",1);
   write_reg("LVDS_tx_sel",0);
   write_reg("plv1_scan_en",0);
 
   write_reg("live_bypass",0);
   write_reg("l1a_bypass",0);
   write_reg("N_width",0x08);
   write_reg("live_bypass_mode",0);
   write_reg("clk_select_mode",0);

   write_reg("pulse_mode2",0);

   bool en_ped_rec = ( GetBoardType()==k_TrigTagADC ) ? false : true;
   write_reg("ena_pedrecord",en_ped_rec);

   // choose fixed pedestal or track pedestal
   write_reg("ped_choose",0xffff);

   DisableDSMode();
   ResetSpill();
   ResetTrigCount();
}

//
//! @brief Reset the spill counter for this FADC.
//
void FADCManager::ResetSpill( void )
{
   write_reg("spill_count_clear",1);
   write_reg("spill_count_clear",0);
}

//
//! @brief Reset the trigger counter for this FADC.
//
void FADCManager::ResetTrigCount( void )
{
   write_reg("trig_count_rst",1);
   write_reg("trig_count_rst",0);
}

//
//! @brief Selection of the DAQ mode to analyze the input data.
//
void FADCManager::SetAnalysisMethod( e14::E_AnalysisMethod method,
                                     e14::E_HitCountFlagState flag )
{
   switch( method )
   {
      case e14::k_VetoSingleThr:
         write_reg("veto_crate",0x1);
         SetSingleThr(true);
         SetTimingWindowDelay( e14::k_VetoBitWindowDelay, e14::k_VetoSingleThr);
         SetQlinMode(true);
         break;

      case e14::k_VetoDualThrWithHitCount:
         write_reg("veto_crate",0x1);
         SetSingleThr(false);
         SetTimingWindowDelay( e14::k_VetoBitWindowDelay, e14::k_VetoDualThrWithHitCount);
         SetDualVetoBit( flag );
         SetQlinMode(false);
         break;

      case e14::k_VetoDualThrWoHitCount:
         write_reg("veto_crate",0x1);
         SetSingleThr(false);
         SetTimingWindowDelay( e14::k_VetoBitWindowDelay, e14::k_VetoDualThrWoHitCount);
         SetDualVetoBit();
         SetQlinMode(false);
         break;

      case e14::k_ET:
         write_reg("veto_crate",0x0);
         SetSingleThr(false);
         SetTimingWindowDelay( e14::k_ClusterBitWindowDelay, e14::k_ET);
         write_reg("delay_cb_readout", e14::k_ClusterBitMapDelay ); 
         SetQlinMode(false);;
         break;

      default:
         std::cout<<"Error in <FADCManager::SetDAQMode> unknown method: "
                  << method << std::endl;
   }
}

//
//! @brief: Perform settings for Daisy Chain.
//! @param[in] nlack How many boards are removed from Daisy Chain. Default value: 0.
//
bool FADCManager::SetDaisyChain( int nlack )
{
   if( !CheckId() ) return false;

   if( nlack<0 || nlack>3 ){
      std::cout<<"Error in <FADCManager::SetDaisyChain> invalid #lack: " << nlack << std::endl;
      return false;
   }   

   const int  rank = E14DAQFunction::GetDaisyChainRank( GetId() );
   if( rank <  nlack ) return true;

   const bool is_first = (rank == nlack) ? true : false;
   const bool en_olrx  = (is_first) ? false : true;
   const int  err_maxt = (rank - nlack + 1) * s_DCErrMaxTime; 
   unsigned long ptn_tx = 0xf000;

   switch( rank )
   {
      case 0:
         write_reg("first_adc", is_first);
         write_reg("lckrefn_l1", en_olrx);
         write_reg("maxt", err_maxt);
         write_reg("pattern_tx", 0xf000);
         break;
      case 1:
         write_reg("first_adc", is_first);
         write_reg("lckrefn_l1", en_olrx);
         write_reg("maxt", err_maxt);
         ptn_tx = (is_first) ? 0xff00 : 0x0f00;
         write_reg("pattern_tx", ptn_tx);
         write_reg("pattern_rx", 0xf000);
         break;
      case 2:
         write_reg("first_adc", is_first);
         write_reg("lckrefn_l1", en_olrx);
         write_reg("maxt", err_maxt);
         ptn_tx = (is_first) ? 0xfff0 : 0x00f0;
         write_reg("pattern_tx", ptn_tx);
         write_reg("pattern_rx", 0xff00);
         break;
      case 3:
         write_reg("first_adc", is_first);
         write_reg("lckrefn_l1", en_olrx);
         write_reg("maxt", err_maxt);
         ptn_tx = (is_first) ? 0xffff : 0x000f;
         write_reg("pattern_tx", ptn_tx);
         write_reg("pattern_rx", 0xfff0);
         break;
      default:
         std::cout<<"Error in <FADCManager::SetDaisyChain>"
                  <<" unexpected Daisy-Chain rank: " << rank << std::endl;
     }
   
   return true;
}

//
//! @brief Set single peak thresholds for this board.
//! @param[in] pvec, the single peak threshold in a vector (for LVDS transmission scheme).
//
bool FADCManager::SetSinglePeakThreshold( std::vector<int> const& pvec )
{
   switch( GetBoardType() )
   {
      case k_125MHzADC:
         return Set1stPeakThresholdFor125MHzADC( pvec );
         break;
      case k_500MHzADC:
         return SetDualPeakThresholdFor500MHzADC( pvec, std::vector<int>() );
         break;
      default:
         return false;
   }
}

//
//! @brief Set two peak threhsolds for this board.
//! @param[in] pvec0, the peak threshold 0 in a vector.
//! @param[in] pvec1, the peak threshold 1 in a vector.
//! @warning The size of both input vectors should be the same as the channels this board has.
//
bool FADCManager::SetDualPeakThreshold( std::vector<int> const& p0vec,
                                        std::vector<int> const& p1vec )
{
   switch( GetBoardType() )
   {
      case k_125MHzADC:
         if( !Set1stPeakThresholdFor125MHzADC( p0vec ) ) return false;
         if( !Set2ndPeakThresholdFor125MHzADC( p1vec ) ) return false;
         break;
      case k_500MHzADC:
         return SetDualPeakThresholdFor500MHzADC( p0vec, p1vec );
         break;
      default:
         return false;
   }
}

//
//! @brief Define the bit locations of two veto results through the DC in veto crate.
//! @param[in] e14::E_CVFlagState is to enable CV hit count transmission with different option.
//! @note  
//!  * This is used for the bit transmission in DC through ADCs, not the final veto bit definition.
//
bool FADCManager::SetDualVetoBit( e14::E_HitCountFlagState flag )
{
   if( !CheckId() ) return false;

   const int rank = E14DAQFunction::GetDaisyChainRank( GetId() );
   const int vb0  = rank * 2;
   const int vb1  = rank * 2 + 1;

   switch( GetBoardType() )
   {
      case k_125MHzADC:
         return SetDualVetoBitFor125MHzADC(vb0, vb1, flag);
         break;
      case k_500MHzADC:
         return SetDualVetoBitFor500MHzADC(vb0, vb1);
         break;
      default:
         return false;
   }
}

//
//! @brief SEt single threshold strategy for this ADC baord.
//! @param[in] isSingleThr = true to set the single threshold strategy.
//
void FADCManager::SetSingleThr( bool isSingleThr )
{
   // is_single_thr is from the register bank (address: 59)
   const int reg_addr = 59;
   write_reg("rb_addr", reg_addr);
   write_reg("rb_value", isSingleThr);
}

//
//! @brief Set the DIGOUT clock phase of ADC
//! @param[in] phase x 60 degree is the phase difference. The valid range is 0-5.
//
void FADCManager::SetDigoutClockPhase( int phase )
{
   // digout_clock_phase is from the register bank (address: 60)
   write_reg("rb_addr", 60);
   write_reg("rb_value", phase);
}

//
//! @brief Setting of the timing window of this board.
//! @param[in] tpair is the std::pair object containing the start time and end time.
//
bool FADCManager::SetTimingWindow( std::pair<int, int> const& tpair )
{
   return SetTimingWindow( tpair.first, tpair.second );
}

//
//! @brief Setting of the timing window of this board.
//! @param[in] tleft and tright is the beginning and the end time of the window. 
//!             0 <= tleft <= 2, 1 0= tright <= 3
//! @warning If tleft = 2 or tright = 3 is set, the peak sensing is invalid.
//
bool FADCManager::SetTimingWindow( int tleft, int tright )
{
   if( tleft>2 || tright>3 || tleft<0 || tright<0 ){
      std::cout<<"Error in <FADCManager::SetTimingWindow>"
               <<" invalid timing parameter tleft: " << tleft
               <<" or tright: " << tright << std::endl;
      return false;
   }

   const int tcenter = 2;   
   write_reg("CB_tmin", tcenter - tleft  );
   write_reg("CB_tmax", tcenter + tright );
   return true;
}

//
//! @brief Set the amount of delay for timing window.
//!        The offset will be added automatically based on the board type and method.
//!
//! @param[in] delay is the center of the window with respect to the raw pulse input.
//
void FADCManager::SetTimingWindowDelay( int delay, e14::E_AnalysisMethod method )
{
   int offset = 0;
   offset += ( method==e14::k_VetoSingleThr ) ? e14::k_DelayOffsetForSingleThr : 0 ;
   offset += ( GetBoardType()==k_125MHzADC )  ? e14::k_DelayOffsetFor125MHzADC : 0 ; 

   if( GetCrateId() == 15 && ( GetSlotId() == 15 || GetSlotId() == 16 || GetSlotId() == 17)) offset = offset+8;
   
   write_reg("CB_delay", delay + offset );
}


void FADCManager::SetPeakScan( int init_delay )
{
  SetTimingWindowDelay( init_delay, e14::k_ET );
   SetTimingWindow(0, 0);

   write_reg("plv1_scan_en",0);
   usleep(1000);
   write_reg("plv1_scan_en",1); 

}

bool FADCManager::SetETLinearFunc( std::vector<int> const& p0vec,
                                   std::vector<int> const& p1vec )
{
   if( GetBoardType()!=k_125MHzADC ) return false;

   if( p0vec.size()!=e14::k_n125MHzChannel || p1vec.size()!=e14::k_n125MHzChannel ){
      std::cout<<"Error in <FADCManager::SetETLinearFunc>"
               <<" p0vec or p1vec size is not " << e14::k_n125MHzChannel << std::endl;
   }

   /// register variable
   unsigned long p0_reg_val;
   unsigned long p1_reg_val;

   /// p0, p1 register addresss offset
   const int p0_addr_offset = 17;
   const int p1_addr_offset = 1;
 
   for( int ich=0; ich<e14::k_n125MHzChannel; ++ich ){

      /// p0 setting ///
      p0_reg_val =  ( p0vec[ich] < 0 ) ? ( 1 << 31 ) : 0; 
      p0_reg_val += abs(p0vec[ich]);
  
      write_reg("rb_addr", ich + p0_addr_offset);
      write_reg("rb_value",p0_reg_val);
      
      /// p1 setting ///
      /// p1 = 0 stands for masking in ET sum

      int et_thr = 0;

      if( p1vec[ich] > 0 ){
         et_thr = ( e14::local_et_thr - p0vec[ich]) / p1vec[ich];
         et_thr = ( et_thr < 0 ) ? 0 : et_thr;
      }else{
         et_thr = 0xffff;
      }

      p1_reg_val = ( et_thr << 16 ) + p1vec[ich];

      write_reg("rb_addr", ich + p1_addr_offset );
      write_reg("rb_value", p1_reg_val );

   }

   return true;
}

//
//! @brief Settings of channel delays.
//! @param[in] delayvec is the integer vector containing the delay of each channel.
//
bool FADCManager::SetChannelDelay( std::vector<int> const& delayvec )
{
   switch( GetBoardType() )
   {
      case k_125MHzADC:
         return SetChannelDelayFor125MHzADC( delayvec );
         break;
      case k_500MHzADC:
         return SetChannelDelayFor500MHzADC( delayvec );
         break;
      default:
         return false;
   }
}

//
//! @brief 
//
bool FADCManager::SetChannelMasking( std::vector<bool> const& maskvec )
{
   switch( GetBoardType() )
   {
      case k_125MHzADC:
         return SetChannelMaskingFor125MHzADC( maskvec );
         break;
      case k_500MHzADC:
         return SetChannelMaskingFor500MHzADC( maskvec );
         break;
      default:
         return false;
   }
}


//
//! @brief Preparation of the VME read-out for this board.
//
void FADCManager::EnableDSMode( void )
{
   // DS_mode: 
   // - bit 0: FIFO writing enable, 
   // - bit 1: FIFO reading request,
   // - bit 2: FIFO NOT clear 
    
   write_reg("DS_mode_control",0x00);
   usleep(1000);
   write_reg("DS_mode_control",0x04);
   write_reg("DS_mode_control",0x05);
}

void FADCManager::DisableDSMode( void )
{
  // DS_mode:
   // - bit 0: FIFO writing enable,
   // - bit 1: FIFO reading request,
   // - bit 2: FIFO NOT clear

   write_reg("DS_mode_control",0x04);
   write_reg("DS_mode_control",0x00);
}

//
//! @brief Collection of raw pulses for pedestal calculations by ADC self triggers. 
//! @warning The crate ID needs to be set to write out the raw file.
//
void FADCManager::CollectPedData( void )
{
   write_reg("use_lossless",0);

   write_reg("N_width",0x06);
   write_reg("live_bypass_mode", 1);
   write_reg("l1a_bypass_mode", 1);
   write_reg("live_bypass", 1);
   write_reg("ena_daq_mode", 0);
   write_reg("l1a_bypass_mode",0);

   const int ntrig = 50;
   for(int i=0; i<ntrig; i++)
   {
      write_reg("pulse_mode2",1);
      write_reg("ena_selftrig",1);
      write_reg("ena_selftrig",0);
      usleep(1000);
   }
}

//
//! @brief Read the pedestal data from the DS buffer through VME interface.
//
void FADCManager::RunDSCaptureForPed( void )
{
   if( GetCrateId()<0 || GetCrateId()>=e14::k_nCrate ){
      std::cout<<"Error in <FADCManager::RunDSCaptureForPed>"
               <<" invalid crate ID: " << GetCrateId() << std::endl;
      return;
   }

   std::ofstream ofile;
   char ofname[100];
   sprintf( ofname, "ped_raw/crate%d/fadc%d.txt", GetCrateId(), GetId() );

   if( !OpenRawOutput(ofile, ofname) ) return;
   FillDSBuffer();
   WriteRawFile( ofile );
   DisableDSMode();
}



//
//! @brief
//
void FADCManager::RunDBCapture( int runId )
{


}

//
//! @brief
//
void FADCManager::RunDBCaptureSpillBase(int runId, int SpillNo)
{
/*
   bool isAlmostFull = false;
   const int MAX_TIMER = 6000000;

   bool isTimeOut = false;

   int timer = 0;

   std::cout<<" Prepare DB capture for run"<< runId <<" , spill" << SpillNo << std::endl;
   //while( !isAlmostFull && read_reg("start(LIVE)")  ){
   while( !isAlmostFull ){
      usleep(1000);
      isAlmostFull = read_reg("almost_full");
      timer++;
      if( timer > MAX_TIMER ){
         isTimeOut = true;
         break;
      }
   }

   write_reg("DS_mode_control",0x04);
   write_reg("DS_mode_control",0x04);
   usleep(1000);
   write_reg("DS_mode_control",0x06);
   write_reg("DS_mode_control",0x06);
  */

   std::cout<<" Start filling data. " << std::endl;
   FillDSBuffer();

   char ofname[100];
   if( SpillNo<0 ){
      if(runId>0 ){
         sprintf( ofname,"/raw_data/raw_data/crate%d/run%d/run%d_fadc%d.txt" 
                        , m_crateId, runId, runId ,m_slotId-3 );
      }
      else{
         sprintf( ofname , "/DAQcodes/e14daq/current/database/ped_data/crate%d/fadc%d.txt"
                         , m_crateId , m_slotId-3 );
      }
   }
   else
      sprintf( ofname,"%s/run%d/run%d_spill%d.txt"
                        , m_dbFileDir.c_str(), runId, runId, SpillNo );


   std::cout<<" Start producing output files: "<< ofname << std::endl;
   std::ofstream ofile( ofname );
   unsigned long data_buffer[16400];
   int nwords = 15480;
   read_blt(0,data_buffer,nwords);

   ofile << std::hex;
   for( int i=0; i<nwords; i++ ){
      ofile << (data_buffer[i]&0x0000ffff) << "\t" << ((data_buffer[i]&0xffff0000)>>16) << "\t";
      if( (i-5) % 8 == 0 ) ofile << std::endl;
   }
  
  ofile << std::endl;
  ofile << "ef";

//  if( isTimeOut )
//     ofile << "time out flag is on! " << std::endl;
/*
  write_reg("DS_mode_control",0x04);
  write_reg("DS_mode_control",0x00);
  write_reg("live_bypass",0x00);
  write_reg("N_width",0x08);
  write_reg("ena_daq_mode",1);
*/

   DisableDSMode();
}

/*
void FADCManager::SetPedMeanSigma( double* mean, double* sigma, double* eta, int* ch_det, DetParContainer* DPC ){
  int NCH = 0;
  double SigmaThr, EtaThr;
  if( GetBoardType()==k_125MHzADC ){  /// 125MHz ADC
    NCH = e14::nChannels; //// 16
  }
  else if( GetBoardType()==k_500MHzADC ){  /// 500MHz ADC
    NCH = e14::n500mhzChannels; //// 4
  }
  else return;

  std::ofstream noisyfile;
  m_runId= DPC->GetRunId();
  noisyfile.open( Form("/raw_data/ped_record/noisy_channel_list/noisych_run%d_crate%d.txt",m_runId,m_crateId) , std::ios::app );
  std::ofstream settingrecord;
  if( settingrecord.is_open() ) {std::cout<<"Setting record file wrongly opened for crate "<<m_crateId<<"\n";}
  settingrecord.open( Form("/raw_data/ped_record/setting_record/crate%d/pedsetting_run%d_crate%d.txt",m_crateId,m_runId,m_crateId) , std::ios::app );
  if( !(settingrecord.is_open()) ) {std::cout<<"Setting record file not opened for crate "<<m_crateId<<"\n";}

  int adc_useps = 0;
  int reg_useps[16], reg_m[16], reg_s[16] = {0};
  int ChannelDetID;
  for( int ich=0; ich<NCH; ich++ ){
    if(ich>=16) break;
    reg_m[ich] = TMath::Nint(mean[ich]);
    reg_s[ich] = (int)(3*sigma[ich]);
    ChannelDetID = ch_det[ich]-1;

    //// for different detectors, different suppresion thresholds and noisy-channel criteira are set
    //// suppression thresholds default is 3*sigma
    //// noisy-channel criteria default is Sigma>4.0 Eta<0.75 for 125MHz, and Sigma>2.0 Eta<0.80 for 500MHz
    //// if cannot find corresponding detector, then set this channel as default
    if( ChannelDetID<0 ) { reg_s[ich] = 99; reg_useps[ich] = 1; SigmaThr = (GetBoardType()==k_125MHzADC) ? 4.0 : 2.0; EtaThr = (GetBoardType()==k_125MHzADC) ? 0.75 : 0.80; }
    else {
      reg_useps[ich] = DPC->GetUsePS( ChannelDetID );
      SigmaThr = DPC->GetSigThr( ChannelDetID );
      EtaThr = DPC->GetEtaThr( ChannelDetID );
      reg_s[ich] = DPC->GetPSThr( ChannelDetID, sigma[ich] );
    }

    //// Judge if this is a noisy channel or a low gain channel, if yes, set this channel's suppresion threshold at 0
    if( sigma[ich]>SigmaThr || eta[ich]<EtaThr ) { reg_s[ich] = 0; noisyfile<<m_crateId<<"\t"<<(m_slotId-3)<<"\t"<<ich<<"\n"; }
  }


  //// Search for any detector in this ADC requires ps
  //// If yes, then turn on ps for this ADC, and if other channel on the same ADC not require ps, set the threshold to 0, to avoid suppression
  for( int ich=0; ich<NCH; ich++ ){
    if(ich>=16) break;
    switch(adc_useps){
      case 0: {
	if( reg_useps[ich]==1 && ch_det[ich]>0 ){
	  adc_useps = 1;
	  ich = -1;
	}
	break;
      }
      case 1: {
	if( reg_useps[ich]==0 && ch_det[ich]>0 ){
	  reg_s[ich] = 0;
	}
	break;
      }
      default: break;
    }
  }


  //// output parameters to file & registers to ADC
  for( int ich=0; ich<NCH; ich++ ){
    if(ich>=16) break;
    //// Add protection, so that no negative values written to registers, Yuting 2021/02/12
    if(mean[ich]<0) {reg_m[ich]=0;}
    if(sigma[ich]<0) {reg_s[ich]=0;}
    if(reg_s[ich]>reg_m[ich]) {reg_s[ich] = reg_m[ich];}
    //// Write suppression parameters to ADC
    write_reg(Form("pedestal_%d",ich), reg_m[ich]);
    write_reg(Form("ps_thr_%d",ich), reg_s[ich]);
    //// Output setting parameters to file
    settingrecord<<(m_slotId-3)<<"\t"<<ich<<"\t"<<(ch_det[ich]-1)<<"\t"<<reg_m[ich]<<"\t"<<mean[ich]<<"   \t"<<reg_s[ich]<<"\t"<<sigma[ich]<<"\n";
  }
  write_reg("use_zero", adc_useps);
  settingrecord<<(m_slotId-3)<<"\t"<<adc_useps<<"\n";

  noisyfile.close();
  settingrecord.close();

}
*/

//
//! @brief Set the data output duplication at the 1st O/L for Q. Lin's test.
//!        This function will be obseleted after finishing the test in Feb. 2020.
//! @param[in] enable is to enable the Qlin mode or not.
//
void FADCManager::SetQlinMode( bool enable )
{
   write_reg("lckrefn_l1", !enable );
   write_reg("lckrefn_l2", enable  );
}

//
//! @brief Set the 1st peak thresholds for 125-MHz ADC.
//! @param[in] pvec The vector contains 16 peak thresholds in the order of channels.
//!            The valid peak threshold range: 0x0000 - 0x3fff.
//! @warning This function can only be applied on 125MHz ADC boards. 
//
bool FADCManager::Set1stPeakThresholdFor125MHzADC( std::vector<int> const& pvec )
{
   if( GetBoardType()!=k_125MHzADC ) return false;
   if( pvec.size()!=s_nChannel125MHzADC ){
      std::cout<<"Error in <FADCManager::Set1stPeakThresholdFor125MHzADC>"
               <<" crate" << GetCrateId() <<" , slot" << GetSlotId()
               <<" : pvec size = " << pvec.size()
               <<" , " << s_nChannel125MHzADC <<" is required. " << std::endl;  
      return false;
   }

   int channelId = 0;
   for(std::vector<int>::const_iterator it=pvec.begin(); it!=pvec.end(); ++it )
   {
      if( *it < 0 ){
         std::cout<<"Warning in <FADCManager::Set1stPeakThresholdFor125MHzADC>"
                  <<" channel" << channelId
                  <<" negative peak height setting: " << *it << std::endl;
      }

      char reg_name[32];
      sprintf( reg_name , "cls_thr_%d" , channelId++ );
      write_reg( reg_name, *it);
   }

   return true;
}

//
//! @brief Settings of the 2nd peak threshold for 125-MHz ADC.
//! @param[in] pvec is the vector contains 16 peak thresholds in the order of channels.
//!                 The valid peak threshold range: 0x0000 - 0x3fff.
//! @warning This function can only be applied on 125MHz ADC boards.
//
bool FADCManager::Set2ndPeakThresholdFor125MHzADC( std::vector<int> const& pvec )
{
   if( GetBoardType()!=k_125MHzADC ) return false;
   if( pvec.size()!=s_nChannel125MHzADC ){
      std::cout<<"Error in <FADCManager::Set2ndPeakThresholdFor125MHzADC>"
               <<" crate" << GetCrateId() <<" , slot" << GetSlotId()
               <<" pvec size = " << pvec.size()
               <<" , " << s_nChannel125MHzADC <<" is required. " << std::endl;
      return false;
   }

   // the 2nd peak thrshold is driven by the register bank from no. 33
   //
   // reg. no.              33, 34,  ..., 40
   // -----------------------------------------
   // [15: 0] is for channel 0,  2,  ..., 14.
   // [31:16] is for channel 1,  3,  ..., 15.

   const unsigned int addr_offset = 33;
   int iaddr = 0;
   for(std::vector<int>::size_type i=0; i<pvec.size(); i=i+2 )
   {
      const int thr0 = pvec[i  ];
      const int thr1 = pvec[i+1];

      if( thr0<0 || thr1<0 ){
         std::cout<<"Warning in <FADCManager::Set2ndPeakThresholdFor125MHzADC>"
                  <<" peak threshold is negative. " << std::endl;
      }

      const unsigned long reg_val = thr0 + ( thr1 << 16 );
      write_reg("rb_addr", addr_offset + iaddr++);
      write_reg("rb_value", reg_val);
   }

   return true;
}

//
//! @brief Settings of the dual peak thresholds for 500MHz ADC.
//! @param[in] pvec is the vector contains 16 peak thresholds in the order of channels.
//! @warning This function can only be applied on 125MHz ADC boards.
//
bool FADCManager::SetDualPeakThresholdFor500MHzADC( std::vector<int> const& p0vec, 
                                                    std::vector<int> const& p1vec )
{
   if( GetBoardType()!=k_500MHzADC ) return false;
   if( p0vec.size()!=s_nChannel500MHzADC ){
      std::cout<<"Error in <FADCManager::SetPeakThresholdFor500MHzADC>"
               <<" crate" << GetCrateId() <<" , slot" << GetSlotId()
               <<" p0vec size = " << p0vec.size()
               <<" , " << s_nChannel500MHzADC <<" is required. " << std::endl;
      return false;
   }

   if( p1vec.size()!=s_nChannel500MHzADC && !p1vec.empty() ){
      std::cout<<"Error in <FADCManager::SetPeakThresholdFor500MHzADC>"
               <<" p1vec size = " << p0vec.size()
               <<" , " << s_nChannel500MHzADC <<" or empty vector is required. " << std::endl;
      return false;
   }

   int channelId = 0;
   for(std::vector<int>::const_iterator it0 = p0vec.begin(); it0!=p0vec.end(); ++it0 )
   {
      if( *it0 < 0 ){
         std::cout<<"Warning in <FADCManager::SetPeakThresholdFor500MHzADC>"
                  <<" channel" << channelId
                  <<" negative peak height from p0vec: " << *it0 << std::endl;
      }

      char reg_name[32];
      sprintf( reg_name , "500MHz_vb_ch%d_thr_0" , channelId++ );
      write_reg( reg_name, *it0 ); 
   }

   channelId = 0;
   for(std::vector<int>::const_iterator it1 = p1vec.begin(); it1!=p1vec.end(); ++it1 )
   {
      if( *it1 < 0 ){
         std::cout<<"Warning in <FADCManager::SetPeakThresholdFor500MHzADC>"
                  <<" crate" << GetCrateId() <<" , slot" << GetSlotId()
                  <<" channel" << channelId
                  <<" negative peak height from p1vec: " << *it1 << std::endl;
      }

      char reg_name[32];
      sprintf( reg_name , "500MHz_vb_ch%d_thr_1" , channelId++ );
      write_reg( reg_name, *it1 );
   }

   return true;
}

//
//! @brief Set dual veto bits for 125-MHz ADC.
//
bool FADCManager::SetDualVetoBitFor125MHzADC( int vb0,
                                              int vb1, 
                                              e14::E_HitCountFlagState flag )
{
   if( vb0<0 || vb0>=e14::k_nBitOfOLWord || vb1<0 || vb1>=e14::k_nBitOfOLWord ){
      std::cout<<"Error in <FADCManager::SetDualVetoBitFor125MHzADC>"
               <<" invalid vb0 or vb1: " << vb0 <<" , " << vb1 << std::endl;
      return false;
   }

   // The veto bit shift register is controlled by reg41 of register bank (rb) in ADC.
   // [3:0] defines vb0 and [7:4] defines vb1
   const unsigned long addr = 41;
   unsigned long val  = ( vb0 | (vb1 << 4) );

   // [9:8] = 1 for type-1 hit count transmission.
   // [9:8] = 2 for type-2 hit count transmission.
   switch( flag )
   {
      case e14::k_NoneHitCountFlag:
         break;
      case e14::k_HitCountType1Flag:
         val += (1 << 8);
         break;
      case e14::k_HitCountType2Flag:
         val += (2 << 8);
         break;
      default:
        std::cout<<"Warning in <FADCManager::SetDualVetoBitFor125MHzADC>"
                 <<" invalid hit count flag value: " << flag << std::endl; 
        return false;
   }

   write_reg("rb_addr", addr);
   write_reg("rb_value", val);    

   return true;
}

//
//! @brief Set dual veto bits for 500-MHz ADC.
//
bool FADCManager::SetDualVetoBitFor500MHzADC( int vb0, int vb1 )
{
   if( vb0<0 || vb0>=e14::k_nBitOfOLWord || vb1<0 || vb1>=e14::k_nBitOfOLWord ){
      std::cout<<"Error in <FADCManager::SetDualVetoBitFor500MHzADC>"
               <<" invalid vb0 or vb1: " << vb0 <<" , " << vb1 << std::endl;
      return false;
   }

   // [3:0] for vb0.
   // [7:4] for vb1
   const unsigned long val = (vb0 | (vb1 << 4) );
   write_reg("500MHz_vb_shift", val);

   return true;
}

//
//! @brief Settings of channel delays for 125-MHz ADC board.
//! @param[in] delayvec is the integer vector. The size should be 16.
//
bool FADCManager::SetChannelDelayFor125MHzADC( std::vector<int> const& delayvec )
{
   if( delayvec.size()!=e14::k_n125MHzChannel ){
      std::cout<<"Error in <FADCManager::SetChannelDelayFor125MHzADC>"
               <<" crate" << GetCrateId() <<" FADC" << GetId() 
               <<": invalid vector size: " << delayvec.size() << std::endl;
      return false;
   }
 
   unsigned long reg_val = 0;
   char          reg_name[32];

   // rules of the delay registers for 125-MHz board
   // ------------------------------------------------
   // [ 3: 0] for channel 0
   // [ 7: 4] for channel 1
   // ..
   // [63:60] for channel 16
   //
   // This is constituted by 4 segments: "delay[15:0]", "delay[31:16]", ..., "delay[63:48]"

   for( int ich=0; ich<e14::k_n125MHzChannel; ++ich )
   {
      if( delayvec[ich]<0 || delayvec[ich]>=16 ){
         std::cout<<"Error in <FADCManager::SetChannelDelayFor125MHzADC>"
                  <<" crate" << GetCrateId() <<" FADC" << GetId()
                  <<": invalid delay: " << delayvec[ich] << std::endl;
         return false;
      }

      int init_bit = (ich / 4 ) * 16;
         
      switch( (ich % 4 ) )
      {
         case 0:
            reg_val = delayvec[ich];
            break;
         case 1:
            reg_val += ( delayvec[ich] << 4 );
            break;
         case 2:
            reg_val += ( delayvec[ich] << 8 );
            break;
         case 3:
            reg_val += ( delayvec[ich] << 12 );
            sprintf(reg_name,"delay[%d:%d]", init_bit+15, init_bit);
            write_reg(reg_name, reg_val);
            break;
         default: 
            ;
      }
   }

   return true;
}
   
//
//! @brief Settings of channel delays for 500-MHz ADC board.
//! @param[in] delayvec is the integer vector. The size should be 4.
//
bool FADCManager::SetChannelDelayFor500MHzADC( std::vector<int> const& delayvec )
{
   if( delayvec.size()!=e14::k_n500MHzChannel ){
      std::cout<<"Error in <FADCManager::SetChannelDelayFor500MHzADC>"
               <<" crate" << GetCrateId() <<" FADC" << GetId()
               <<": invalid vector size: " << delayvec.size() << std::endl;
      return false;
   }

   // rules of the delay registers for 500-MHz board
   // ------------------------------------------------
   // [ 4: 0] for channel 0
   // [ 9: 5] for channel 1
   // ..
   // [19:15] for channel 3
   //
   // This is constituted by 4 segments: "delay[15:0]", "delay[31:16]", ..., "delay[63:48]"

   unsigned long reg_val = 0;
   for( int ich=0; ich<delayvec.size(); ++ich )
   {
      if( delayvec[ich]<0 || delayvec[ich]>=32 ){
         std::cout<<"Error in <FADCManager::SetChannelDelayFor500MHzADC>"
                  <<" crate" << GetCrateId() <<" FADC" << GetId()
                  <<": invalid delay: " << delayvec[ich] << std::endl;
         return false;
      }

      reg_val += ( delayvec[ich] ) << ( ich * 5 );
   }

   write_reg("delay[15:0]", ( reg_val & 0xffff) );
   write_reg("delay[31:16]", ( (reg_val & 0x1f0000) >> 16) );

   return true;
}

//
//! @brief Setting of channel masking for 125-MHz ADC board.
//! @param[in] maskvec is the boolean vector for masking. The size should be 16. 
//
bool FADCManager::SetChannelMaskingFor125MHzADC( std::vector<bool> const& maskvec )
{
   if( maskvec.size()!=e14::k_n125MHzChannel ){
      std::cout<<"Error in <FADCManager::SetChannelMaskingFor125MHzADC>"
               <<" crate" << GetCrateId() <<" FADC" << GetId()
               <<": invalid vector size: " << maskvec.size() << std::endl;
      return false;
   }   

   for( int ich=0; ich<maskvec.size(); ++ich )
   {
      char reg_name[32];
      sprintf( reg_name, "cls_mask_%d", ich );
      write_reg(reg_name, maskvec[ich]);
   }

   return true;
}
   
//
//! @brief Setting of channel masking for 500-MHz ADC board.
//! @param[in] maskvec is the boolean vector for masking. The size should be 4.
//
bool FADCManager::SetChannelMaskingFor500MHzADC( std::vector<bool> const& maskvec )
{
   if( maskvec.size()!=e14::k_n500MHzChannel ){
      std::cout<<"Error in <FADCManager::SetChannelMaskingFor500MHzADC>"
               <<" crate" << GetCrateId() <<" FADC" << GetId()
               <<": invalid vector size: " << maskvec.size() << std::endl;
      return false;
   }

   for( int ich=0; ich<maskvec.size(); ++ich )
   {
      char reg_name[32];
      sprintf( reg_name, "500MHz_vb_ch%d_mask", ich );
      write_reg(reg_name, maskvec[ich]);
   }

   return true;
}

//
//! @brief Preparation of a raw pulse output file.
//! @param[in] ofile is an std::ofstream object and name is the file name.
//
bool FADCManager::OpenRawOutput( std::ofstream &ofile, char const* name )
{
   char const* rawdir = std::getenv("VME_OUT_DIR");
   if( rawdir==NULL ){
      std::cout<<"Error in <FADCManager::OpenRawOutput>"
               <<" VME_OUT_DIR is not set." << std::endl;
      return false;
   }

   char ofname[100];
   sprintf(ofname, "%s/%s", rawdir, name );
   ofile.open( ofname );
   if( ofile.fail() ){
      std::cout<<"Error in <FADCManager::OpenRawOutput>"
               <<" fail to open file:. " << ofname << std::endl;
      return false;
   }

   return true;
}

//
//! @brief Filling events in the buffer zone before reading it.
//! @return The validity of the data in the buffer zone.
//! @warning The DB mode needs to be set in advance.
//
bool FADCManager::FillDSBuffer()
{
   const int MaxFillTime = 6000000;
   bool almost_full = false;
   
   int timer = 0;

   while( !almost_full ){
      usleep(1000);
      almost_full = read_reg("almost_full");

      if( timer++ > MaxFillTime ) return false ;
   }

   // DS_mode:
   // - bit 0: FIFO writing enable,
   // - bit 1: FIFO reading request,
   // - bit 2: FIFO NOT clear
   write_reg("DS_mode_control",0x04);
   write_reg("DS_mode_control",0x04);
   usleep(1000);
   write_reg("DS_mode_control",0x06);
   write_reg("DS_mode_control",0x06);

   return true;
}

//
//! @brief Writing out the raw file via an std::ofstream object.
//! @param[in] ofile is an std::ofstream object.
//! @warning ofile must be openable.
//
void FADCManager::WriteRawFile( std::ofstream& ofile )
{
   const int nword = 15480;
   unsigned long data_buffer[nword];

   read_blt(0, data_buffer, nword);
   ofile << std::hex;

   for( int iword=0; iword<nword; ++iword )
   {
      ofile << ( (data_buffer[iword] & 0x0000ffff )       ) << "\t" 
            << ( (data_buffer[iword] & 0xffff0000 ) >> 16 ) << "\t";

      if( (iword-5) % 8 == 0 ) ofile << std::endl;
   }

  ofile << std::endl;
  ofile << "ef";
}

//
//! @brief Check of the ID value.
//
bool FADCManager::CheckId( void ) const
{
   if( m_id<0 ){
      std::cout<<"Warning in <FADCManager::CheckId>"
               <<" the board ID is not properly intialized. " << std::endl;
      return false;
   }

   return true;
}
