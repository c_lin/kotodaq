#include "VetoFanoutCDTManager.h"

//
//! @brief Constructor initialized by slot number.
//! @param[in] slotId is the slot number.
//
VetoFanoutCDTManager::VetoFanoutCDTManager( int slotId ) 
   : VMEController( slotId, "fcdt")
{
   Init();
}

//
//! @brief Default destructor.
//
VetoFanoutCDTManager::~VetoFanoutCDTManager()
{
   ;
}

//
//! @brief Initialization of this board.
//
void VetoFanoutCDTManager::Init( void )
{
   SetTestMode(true);
   Reset();

   write_reg("ena_sim_trig",0x0);
   write_reg("lvds_in_switch",0x0);
   write_reg("ena_digout_debug",0x0);

   write_reg("tlk_err_max_time",e14::fanout_tlk_err_maxt);
   write_reg("dc_err_max_time",e14::fanout_dc_err_maxt);

   write_reg("err_masking",e14::veto_masking);
}
