#include "SettingFile.h"

//
//! @brief Constructor initialized by crated ID.
//
SettingFile::SettingFile( int crateId )
    : m_crateId( crateId )
{
   Init();
}

//
//! @brief Default destructor
//
SettingFile::~SettingFile()
{
  ;
}

//
//! @brief Initisalization of container
//
void SettingFile::Init()
{
   if( m_crateId<0 || m_crateId >= 18 ){
      std::cout <<"Error in <SettingFile::Init>"
           <<" invalid crate ID: "<< m_crateId << std::endl;
      }
}

//
//! @brief Loading of the Detector and ModID from an external file.
//
bool SettingFile::LoadDetMapFile( void )
{
   std::ostringstream ifname;
   ifname  << "MapFile/Crate"<< m_crateId << "_DetectorMap.txt";
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname.str().c_str()) ) return false;

   int nfadc = 16; // get fadc number 

   std::vector<std::vector<std::string> > detvec(nfadc);
   std::vector<std::vector<int> > modvec(nfadc);

   int r_fadcId, r_chId, r_modId;
   std::string r_det;
   while( ifile >> r_fadcId >> r_chId >> r_det >> r_modId ){
      // check adc 
      // check Sequence
      detvec[r_fadcId].push_back( r_det );
      modvec[r_fadcId].push_back( r_modId );
   } 

   m_detvec.swap(detvec);
   m_modvec.swap(modvec);

   return true;
}

//
//! @brief Export setting file.
//! @param[in] name is the file address under DAQ_SETTING_DIR.
//! @param[in] ofile is the std::ofstream object to export.
//! @return The correctness of the file open.
//
bool SettingFile::ExportSettingFile( std::ofstream &ofile, char const* name )
{
   char const* setdir = std::getenv("DAQ_SETTING_DIR");
   if( setdir==NULL ){
      std::cout<<"Error in <SettingFile::ExportSettingFile>"
               <<" DAQ_SETTING_DIR is not set." << std::endl;
      return false;
   }

   std::ostringstream ofname;
   ofname << setdir <<"/"<< name;
   ofile.open( ofname.str().c_str() );
   if( ofile.fail() ){
      std::cout<<"Error in <SettingFile::ExportSettingFile>"
               <<" fail to export file: " << ofname.str() << std::endl;
      return false;
   }
   return true;
}

//
//! @brief 
//
bool SettingFile::LoadBitAddressFile( void )
{
   std::ostringstream ifname;
   ifname  << "MapFile/Crate_DetectorMap.txt";
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname.str().c_str()) ) return false;

   int nfadc = 16; // get fadc number 

   std::vector<std::vector<std::string> > detvec(nfadc);
   std::vector<std::vector<int> > modvec(nfadc);

   int r_fadcId, r_chId, r_modId;
   std::string r_det;
   while( ifile >> r_fadcId >> r_chId >> r_det >> r_modId ){
      // check adc 
      // check Sequence
      detvec[r_fadcId].push_back( r_det );
      modvec[r_fadcId].push_back( r_modId );
   } 

   m_detvec.swap(detvec);
   m_modvec.swap(modvec);

   return true;
}
