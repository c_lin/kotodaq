#include "LocalCrateSettingHandler.h"

//
//! @brief Constructor initialized by crate ID.
//
LocalCrateSettingHandler::LocalCrateSettingHandler( int crateId )
   : m_crateId( crateId )
{
   Init();
}

//
//! @brief Default destructor
//
LocalCrateSettingHandler::~LocalCrateSettingHandler()
{
   ;
}

//
//! @brief Initialization of container
//
void LocalCrateSettingHandler::Init()
{
   m_isError = false;
   if( m_crateId<0 || m_crateId>=e14::k_nCrate ){
      std::cout<<"Error in <LocalCrateSettingHandler::Init>"
               <<" invalid crate ID: " << m_crateId << std::endl;
      m_isError = true;
   }

   if( !IsError() ) LoadSlot();
   m_analysisMethod = ( IsError() ) ? e14::k_UndefinedMethod 
                                    : e14::k_CrateAnalysisMethod[ m_crateId ];

   m_err_pair = std::make_pair(-9999, -9999);
}

//
//! @brief Loading of the peak thresholds from an external file.
//!
//! The external file should be located at 
//! : DAQ_SETTING_DIR/PeakThreshold/type/Crate%d_PeakThreshold.txt
//!
//! Format:
//! "FADC ID"	"Channel ID"	"Threshold 0"	"Threshold 1"
//! 0		0		?		?
//! 0		1		?		?
//! ..
//! 0		15		?		?
//! 1		0		?		?
//! ..
//! 15		15		?		? 
//!
//! If the single veto stategy is adopted, the threshold 1 column should be removed.
//! The number of rows = 16 (#FADCs) x 16 (#channels) = 256.
//!
//! @param[in] type is the run type.
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadPeakThresholdFromExternalFile( char const* type )
{
   char ifname[100];
   sprintf( ifname, "PeakThreshold/%s/Crate%d_PeakThreshold.txt", type, m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   switch( GetAnalysisMethod() )
   {
      case e14::k_VetoDualThrWithHitCount:
      case e14::k_VetoDualThrWoHitCount:
         return LoadDualPeakThresholdFromExternalFile(ifile);
         break;
      case e14::k_VetoSingleThr:
      case e14::k_ET:
         return LoadSinglePeakThresholdFromExternalFile(ifile);
         break;
      default:
         return false;
   }
}

//
//! @brief Loading of the ET linear functions (p0, p1) from an external file. 
//!
//! The external file should be located at 
//! : DAQ_SETTING_DIR/ETLinearFunc/Crate%d_ETLinearFunc.txt
//!
//! Format:
//! "FADC ID"   "Channel ID"    "p0"	"p1"
//! 0           0               ?       ?
//! 0           1               ?       ?
//! ..
//! 0           15              ?       ?
//! 1           0               ?       ?
//! ..
//! 15          15              ?       ?
//!
//! The number of rows = 16 (#FADCs) x 16 (#channels) = 256.
//!
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadETLinearFuncFromExternalFile( void )
{
   if( GetAnalysisMethod()!=e14::k_ET ) return false;
   char ifname[100];
   sprintf( ifname, "ETLinearFunc/Crate%d_ETLinearFunc.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   const int nfadc = GetnFADC();
   std::vector<std::vector<int> > etp0vec( nfadc ), etp1vec( nfadc );

   int r_fadcId, r_chId, r_p0, r_p1;
   while( ifile >> r_fadcId >> r_chId >> r_p0 >> r_p1 )
   {
      if( !CheckFADCIdValidity(r_fadcId, "ETLinearFunc") ) return false;
      if( !CheckSequence(etp0vec[r_fadcId].size(), r_chId, "ETLinearFunc") ) return false;

      etp0vec[r_fadcId].push_back(r_p0);
      etp1vec[r_fadcId].push_back(r_p1);
   }

   m_etp0vec.swap( etp0vec );
   m_etp1vec.swap( etp1vec );

   return true;
}

//
//! @brief Loading of the channel delays from an external file.
//!
//! The external file should be located at 
//! : DAQ_SETTING_DIR/ChannelDelay/Crate%d_ChannelDelay.txt
//!
//! Format:
//! "FADC ID"   "Channel ID"    "delay"
//! 0           0               ?               
//! 0           1               ?               
//! ..
//! 0           15              ?               
//! 1           0               ?               
//! ..
//! 15          15              ?               
//!
//! If the single veto stategy is adopted, the threshold 1 column should be removed.
//! The number of rows = 16 (#FADCs) x 16 (#channels) = 256.
//!
//! @param[in] type is the run type.
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadChannelDelayFromExternalFile( void )
{
   char ifname[100];
   sprintf( ifname, "ChannelDelay/Crate%d_ChannelDelay.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   const int nfadc = GetnFADC();
   std::vector<std::vector<int> > delayvec( nfadc );;

   int r_fadcId, r_chId, r_delay;
   while( ifile >> r_fadcId >> r_chId >> r_delay )
   {
      if( !CheckFADCIdValidity(r_fadcId, "ChannelDelay") ) return false;
      if( !CheckSequence(delayvec[r_fadcId].size(), r_chId, "ChannelDelay") ) return false;

      delayvec[r_fadcId].push_back(r_delay);
   }

   m_delayvec.swap( delayvec );

   return true;
}
   
//
//! @brief Loading of the channel masking from an external file.
//!
//! The external file should be located at 
//! : DAQ_SETTING_DIR/ChannelMasking/type/Crate%d_ChannelMasking.txt
//!
//! Format:
//! "FADC ID"   "Channel ID"    "masking"
//! 0           0               ?               
//! 0           1               ?               
//! ..
//! 0           15              ?               
//! 1           0               ?               
//! ..
//! 15          15              ?               
//!
//! The number of rows = 16 (#FADCs) x 16 (#channels) = 256.
//!
//! @param[in] type is the run type.
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadChannelMaskingFromExternalFile( char const* type )
{
   char ifname[100];
   sprintf( ifname, "ChannelMasking/%s/Crate%d_ChannelMasking.txt", type, m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   const int nfadc = GetnFADC();
   std::vector<std::vector<bool> > maskvec( nfadc );

   int r_fadcId, r_chId;
   bool r_mask;
   while( ifile >> r_fadcId >> r_chId >> r_mask)
   {
      if( !CheckFADCIdValidity(r_fadcId, "ChannelMasking") ) return false;
      if( !CheckSequence(maskvec[r_fadcId].size(), r_chId, "ChannelMasking") ) return false;

      maskvec[r_fadcId].push_back(r_mask);
   }

   m_maskvec.swap(maskvec);

   return true;
}
   
//
//! @brief Loading of the timing window settings from an external file.
//!
//! The external file should be located at
//! : DAQ_SETTING_DIR/TimingWindow/type/Crate%d_TimingWindow.txt
//!
//! Format:
//! "FADC ID"   "window begin"	"window end"
//! 0           ?               ?
//! 1           ?               ?
//! ..
//! 15          ?              ?
//!
//! The number of rows = 16 (#FADCs).
//!
//! @param[in] type is the run type.
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadTimingWindowFromExternalFile( char const* type )
{
   char ifname[100];
   sprintf( ifname, "TimingWindow/%s/Crate%d_TimingWindow.txt", type, m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   std::vector<std::pair<int, int> > tvec;

   int r_fadcId, r_tbeg, r_tend;
   while( ifile >> r_fadcId >> r_tbeg >> r_tend )
   {
      if( !CheckFADCIdValidity(r_fadcId, "TimingWindow") ) return false;
      if( !CheckSequence(tvec.size(), r_fadcId, "TimingWindow") ) return false;

      tvec.push_back( std::make_pair(r_tbeg, r_tend) );
   }

   m_tvec.swap( tvec );

   return true;
}

//
//! @brief Loading of the Daisy Chain settings from an external file.
//!
//! The external file should be located at
//! : DAQ_SETTING_DIR/DaisyChain/Crate%d_DaisyChain.txt
//!
//! Format:
//! "DC input"	"#lack"
//! 0		?
//! 1		?
//! ..
//! 3		?
//! 
//! The number of rows is the same as of number of Daisy Chain inputs.
//!
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadDaisyChainFromExternalFile( void )
{
   if( GetAnalysisMethod()==e14::k_VetoSingleThr ) return false;
   char ifname[100];
   sprintf( ifname, "DaisyChain/Crate%d_DaisyChain.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   std::vector<int> lackvec;

   int r_in, r_nlack;
   while( ifile >> r_in >> r_nlack )
   {
      if( !CheckSequence(lackvec.size(), r_in, "DaisyChain") ) return false;
      lackvec.push_back( r_nlack );
   }

   m_lackvec.swap( lackvec );

   return true;
}

//
//! @brief Loading of both hit count and flag from an external file.
//! @return The corretness of the file loading.
//
bool LocalCrateSettingHandler::LoadHitCountSettingFromExternalFile( void )
{
//   if( !LoadHitCountRangeFromExternalFile() ) return false;
   if( !LoadHitCountFlagFromExternalFile() ) return false;
   return true;
}
  
// 
//! The external file should be located at
//! : DAQ_SETTING_DIR/HitCount/Crate%d_HitCountRange.txt
//!
//! Format:
//! "DC input"  "min. #hit"	"max. #hit"
//! 0           ?		?
//! 1           ?		?
//!
//! The number of rows is expected to be 2 (#results of hit count).
//!
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadHitCountRangeFromExternalFile( void )
{
   if( GetAnalysisMethod()!=e14::k_VetoDualThrWithHitCount ) return false;
   char ifname[100];
   sprintf( ifname, "HitCount/Crate%d_HitCountRange.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   std::vector<std::pair<int, int> > nhitvec;

   int r_in, r_min, r_max;
   while( ifile >> r_in >> r_min >> r_max )
   {
      if( !CheckSequence( nhitvec.size(), r_in, "HitCountRange") ) return false;
      nhitvec.push_back( std::make_pair(r_min, r_max) );
   }

   m_nhitvec.swap( nhitvec );

   return true;
}

//
//! The external file should be located at
//! : DAQ_SETTING_DIR/HitCount/Crate%d_HitCountFlag.txt
//!
//! Format:
//! "FADC ID"  "flag"
//! 0           ?
//! 1           ?
//! ..
//! 15           ?
//!
//! flag = 0 (hit count is inactivated), 1 (type 1 hit count), 2 (type 2 hit count).
//! The number of rows is expected to be the same as the number of ADCs in this crate..
//!
//! @return The correctness of the file loading.
//
bool LocalCrateSettingHandler::LoadHitCountFlagFromExternalFile( void )
{
   if( GetAnalysisMethod()!=e14::k_VetoDualThrWithHitCount ) return false;
   char ifname[100];
   sprintf( ifname, "HitCount/Crate%d_HitCountFlag.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   std::vector<e14::E_HitCountFlagState> hitflagvec;

   int r_in, r_flag;
   while( ifile >> r_in >> r_flag )
   {
      if( !CheckSequence( hitflagvec.size(), r_in, "HitCountFlag") ) return false;
      hitflagvec.push_back( static_cast<e14::E_HitCountFlagState>(r_flag) );
   }

   m_hitflagvec.swap( hitflagvec );

   return true;
}

//
//! @brief The vector of 1st peak threshold set.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return null vector for the invalid FADC ID or the external file is not loaded yet.
//! @warning LoadPeakThresholdFromExternalFile() needs to be called in advance.
//
std::vector<int> const& 
LocalCrateSettingHandler::Get1stPeakThresholdVector( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_pthr0vec.size() ) ? m_null_intvec 
                                                   : m_pthr0vec[fadcId];
}

//
//! @brief The vector of 2nd peak threshold set.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return null vector for the invalid FADC ID or the external file is not loaded yet.
//! @warning The LoadPeakThresholdFromExternalFile() needs to be called in advance.
//
std::vector<int> const& 
LocalCrateSettingHandler::Get2ndPeakThresholdVector( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_pthr1vec.size() ) ? m_null_intvec
                                                   : m_pthr1vec[fadcId];
}

//
//! @brief The vector of p0 of ET linear function.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return null vector for the invalid FADC ID or the external file is not loaded yet.
//! @warning The LoadETLinearFuncFromExternalFile() needs to be called in advance.
//
std::vector<int> const& 
LocalCrateSettingHandler::GetETLinearFuncP0Vector( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_etp0vec.size() ) ? m_null_intvec
                                                  : m_etp0vec[fadcId];
}

//
//! @brief The vector of p1 of ET linear function.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return null vector for the invalid FADC ID or the external file is not loaded yet.
//! @warning The LoadETLinearFuncFromExternalFile() needs to be called in advance.
//
std::vector<int> const& 
LocalCrateSettingHandler::GetETLinearFuncP1Vector( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_etp1vec.size() ) ? m_null_intvec
                                                  : m_etp1vec[fadcId];
}

//
//! @brief The vector of channel delays.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return null vector for the invalid FADC ID or the external file is not loaded yet.
//! @warning The LoadChannelDelayFromExternalFile() needs to be called in advance.
//
std::vector<int> const& 
LocalCrateSettingHandler::GetChannelDelayVector( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_delayvec.size() ) ? m_null_intvec
                                                   : m_delayvec[fadcId];
}
   
//
//! @brief The vector of channel masking.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return invalid FADC ID or the external file is not loaded yet.
//! @warning The LoadChannelMaskingFromExternalFile() needs to be called in advance.
//
std::vector<bool> const& 
LocalCrateSettingHandler::GetChannelMaskingVector( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_maskvec.size() ) ? m_null_boolvec
                                                  : m_maskvec[fadcId];
}

//
//! @brief The pair of the timing window.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return (-9999, -9999) is returned if fdacId is invalid or the external file is not loaded yet.
//! @arning The LoadTimingWindowFromExternalFile() needs to be called in advance.
//
std::pair<int, int> const&
LocalCrateSettingHandler::GetTimingWindowPair( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_tvec.size() ) ? m_err_pair
                                               : m_tvec[fadcId];
}

//
//! @brief The amount of lack in a Daisy Chain.
//! @param[in] fadcId is the FADC ID.
//! @return -1 for the invalid FADC IS or the external file is not loaded yet.
//! @warning The LoadTimingWindowFromExternalFile() needs to be called in advance.
//
int LocalCrateSettingHandler::GetnLackOfDaisyChain( int fadcId ) const
{
   int dcId = E14DAQFunction::GetDaisyChainId( fadcId );
   return ( dcId < 0 || dcId>=m_lackvec.size() ) ? -1 : m_lackvec[dcId];
}

//
//! @brief Hit count flag of each FADC board.
//! @param[in] fadcId is counted from 0.
//! @return 0: hit count is not activated, 1: type1 hit count, 2: type2 hit count
//
e14::E_HitCountFlagState
LocalCrateSettingHandler::GetHitCountFlag( int fadcId ) const
{
   return (fadcId<0 || fadcId>=m_hitflagvec.size() ) ? e14::k_NoneHitCountFlag 
                                                     : m_hitflagvec[fadcId];
}

//
//! @brief Loading of board slot configuration from an external file.
//!
//! The external file should be located at DAQ_SETTING_DIR/ADCSlot
//! The file name is Crate%d_Slot.txt and the format is required as follows:
//
//! "ADC ID"	"slot number"
//! 0		3
//! 1		4
//! ...
//! 15		?
//!
//! The number of rows depend on how many boards are inserted in this crate.
//
bool LocalCrateSettingHandler::LoadSlot()
{
   char ifname[100];
   sprintf( ifname, "ADCSlot/Crate%d_Slot.txt", m_crateId);
   std::ifstream ifile;
   if( !E14DAQFunction::LoadSettingFile(ifile, ifname) ) return false;

   int r_id, r_slot;
   std::vector<int> slotvec;
   while( ifile >> r_id >> r_slot )
      slotvec.push_back(r_slot);

   m_slotvec.swap(slotvec);

   return true;
}

//
//! @brief Loading of the single thresholds from an external file.
//! @param[in] ifile is the std::ifstream object of the external file.
//! @return The correctness of the contents in this external file.
//
bool LocalCrateSettingHandler::LoadSinglePeakThresholdFromExternalFile( std::ifstream& ifile )
{
   const int nfadc = GetnFADC();
   std::vector<std::vector<int> > pthr0vec( nfadc );

   int r_fadcId, r_chId, r_thr0;
   while( ifile >> r_fadcId >> r_chId >> r_thr0 )
   {
      if( !CheckFADCIdValidity(r_fadcId, "SinglePeakThreshold") ) return false;
      if( !CheckSequence(pthr0vec[r_fadcId].size(), r_chId, "SinglePeakThreshold") ) return false;

      pthr0vec[r_fadcId].push_back(r_thr0);
   }

   m_pthr0vec.swap( pthr0vec );

   return true;
}

//
//! @brief Loading of the dual thresholds from an external file.
//! @param[in] ifile is the std::ifstream object of the external file.
//! @return The correctness of the contents in this external file. 
//
bool LocalCrateSettingHandler::LoadDualPeakThresholdFromExternalFile( std::ifstream& ifile )
{
   const int nfadc = GetnFADC();
   std::vector<std::vector<int> > pthr0vec( nfadc ), pthr1vec( nfadc );

   int r_fadcId, r_chId, r_thr0, r_thr1;
   while( ifile >> r_fadcId >> r_chId >> r_thr0 >> r_thr1 )
   {
      if( !CheckFADCIdValidity(r_fadcId, "DualPeakThreshold") ) return false;
      if( !CheckSequence(pthr0vec[r_fadcId].size(), r_chId, "DualPeakThreshold") ) return false;

      pthr0vec[r_fadcId].push_back(r_thr0);
      pthr1vec[r_fadcId].push_back(r_thr1);
   }

   m_pthr0vec.swap( pthr0vec );
   m_pthr1vec.swap( pthr1vec );

   return true;
}

//
//! @brief Check of the input FADC ID.
//! @param[in] fadcId is the FADC ID counted from 0.
//! @return The validity of this ID.
//
bool LocalCrateSettingHandler::CheckFADCIdValidity( int fadcId, char const* label ) const
{
   if( fadcId<0 || fadcId>=GetnFADC() ){
      std::cout<<"Error in <LocalCrateSettingHandler::CheckFADCIdValidity>"
               <<" [" << label <<"]"
               <<" crate" << m_crateId
               <<" : invalid FADC ID: " << fadcId << std::endl;
      return false;
   }

   return true;
}

//
//! @brief Check of the sequence of the input ID by vector size.
//! @param[in] size is the vector size to be checked. id is for eximination.
//! @return The channel ID is reasonable or not.
//
bool LocalCrateSettingHandler::CheckSequence( unsigned int size, int id, char const* label ) const
{
   if( size!=id ){
      std::cout<<"Error in <LocalCrateSettingHandler::CheckSequence>"
               <<" [" << label <<"]"
               <<" crate" << m_crateId
               <<" : the contents are not in the sequence of ID. " << std::endl;
      return false;
   }

   return true;
}
