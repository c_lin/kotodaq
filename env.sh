##################################
# env for e14daq library setting #
##################################

export E14DAQLIB=/misc/dev

export DAQ_SETTING_DIR=${E14DAQLIB}/setting
export VME_OUT_DIR=/DAQcodes/e14daq/vme_out
export ERR_OUT_DIR=/DAQcodes/e14daq/log/error
export CNT_OUT_DIR=/DAQcodes/e14daq/log/count
export DUMP_OUT_DIR=/DAQcodes/e14daq/log/run_dump
export DAQ_LOG_DIR=/DAQcodes/e14daq/log

##################################
export E14DAQ_CONFIG=${E14DAQLIB}/config
export LD_LIBRARY_PATH=${E14DAQLIB}/source/lib:$LD_LIBRARY_PATH
